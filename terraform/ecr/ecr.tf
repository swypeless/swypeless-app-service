resource "aws_ecr_repository" "app-service" {
  name = "app-service"
  image_tag_mutability = "MUTABLE"
  image_scanning_configuration {
    scan_on_push = true
  }
  tags = {
    service-visibility = "devops"
    service-type = "artifacts"
    service-name = "app-service"
    service-arch = "docker"
  }
}
resource "aws_ecr_lifecycle_policy" "app-service" {
  policy = file("${path.module}/policies/ecr_lifecycle_policy.json")
  repository = aws_ecr_repository.app-service.name
}