variable "aws_account" {}

locals {
  security_group_name = "${terraform.workspace}--lb_security_group__app-service"
}

module "swypeless-data" {
  source = "git@bitbucket.org:swypeless/swypeless-tf-modules.git//swypeless-data"
  aws_account = var.aws_account
  aws_region = var.aws_region
  env_name = terraform.workspace
  load_network = true
  load_s3-access-logs = true
}

resource "aws_security_group" "lb-sg" {
  name = local.security_group_name
  description = "${terraform.workspace}: App Service load balancer security group"
  vpc_id = module.swypeless-data.network.vpc.id

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    description = "Allow http traffic"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    description = "Allow https traffic"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol = -1
    from_port = 0
    to_port = 0
    description = "Allow egress to anywhere"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = local.security_group_name
    service-name = "app-service"
    service-visibility = "public"
    service-env = terraform.workspace
    service-type = "http"
  }
}

resource "aws_lb" "this" {
  name = "${terraform.workspace}--lb-app-service"
  internal = false
  load_balancer_type = "application"
  security_groups = [aws_security_group.lb-sg.id]
  subnets = module.swypeless-data.network.subnet-ids__public.ids

  access_logs {
    bucket = module.swypeless-data.s3__access-logs.s3-bucket.id
    enabled = true
  }
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.this.arn
  port = "80"
  protocol = "HTTP"
  default_action {
    type = "redirect"
    redirect {
      port = "443"
      protocol = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "https" {
  load_balancer_arn = aws_lb.this.arn
  port = "443"
  protocol = "HTTPS"
  ssl_policy = "ELBSecurityPolicy-2016-08"
  certificate_arn = module.swypeless-data.network.ssl-cert__swypeless.arn
  default_action {
    type = "fixed-response"
    fixed_response {
      content_type = "text/plain"
      message_body = "Not Found"
      status_code = "404"
    }
  }
}

resource "aws_route53_record" "this" {
  count = terraform.workspace == "production" || terraform.workspace == "stage" ? 1 : 0
  zone_id = module.swypeless-data.network.route53-zone__swypeless.zone_id
  name = terraform.workspace == "production" ? "app.${module.swypeless-data.network.route53-zone__swypeless.name}" : "app-stage.${module.swypeless-data.network.route53-zone__swypeless.name}"
  type = "CNAME"
  ttl = terraform.workspace == "production" ? "300" : "60"
  records = [
    aws_lb.this.dns_name
  ]
}