variable "aws_account" {}
variable "aws_ecr_registry" {}

resource "aws_iam_role_policy" "this" {
  name   = "${terraform.workspace}-service-policy__app-service"
  policy = file("${path.module}/policies/app-service-role-policy.json")
  role   = module.app-service.role__service.name
}

module "swypeless-data" {
  source = "git@bitbucket.org:swypeless/swypeless-tf-modules.git//swypeless-data"
  aws_account = var.aws_account
  aws_region = var.aws_region
  env_name = terraform.workspace
  load_network = true
}

data template_file "app-service" {
  template = file("${path.module}/service__app.json")
  vars = {
    SERVICE_ENV = terraform.workspace
    AWS_ACCOUNT = var.aws_account
    AWS_REGION = var.aws_region
    AWS_ECR_REGISTRY = var.aws_ecr_registry
    JWT_EXPIRES_IN = terraform.workspace == "production" ? "5m" : "15m"
    INTERNAL_URL_ROOT = module.swypeless-data.network.route53-zone__swypeless-internal.name
    LOG_GROUP = module.app-service.log_group
  }
}

locals {
  desired-count = terraform.workspace == "production" ? 1 : 1
  max-count = terraform.workspace == "production" ? 16 : 4
  // https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task-cpu-memory-error.html
  // 256 = .25 vCPU
  // 512 = .50 vCPU
  // 1024 = 1.0 vCPU
  cpu = terraform.workspace == "production" ? 1024 : 512
  memory = terraform.workspace == "production" ? 2048 : 1024
  min-percent = terraform.workspace == "production" ? 50 : 100 # in stage desired is 1 so 1*1=1, prod 2*0.5=1
  max-percent = terraform.workspace == "production" ? 500 : 300
}

module "app-service" {
  source = "git@bitbucket.org:swypeless/swypeless-tf-modules.git//ecs-microservice"

  aws_account = var.aws_account
  aws_region = var.aws_region

  // service_* are used to create names and tags
  service_name = "app-service"
  service_env = terraform.workspace
  service_arch = "node"
  service_os = "docker"

  service_visibility = "public"

  container_definitions = data.template_file.app-service.rendered
  // there is also a container_port that defaults to 3000 (express/nestjs etc all default to 3000)
  exposed__port = 3000
  container__port = 3000

  service__desired-count = local.desired-count
  service__deployment-maximum-percent = local.max-percent
  service__deployment-minimum-healthy-percent = local.min-percent
  service__cpu = local.cpu
  service__memory = local.memory

  // This is used to build the route in the loadbalancer. The microservice should also be serving off of this path
  // since the load balancer doesn't strip info off.
  service__base-path = ""

  service__security-groups = []
  lb__slow-start = 30
  lb__name = "${terraform.workspace}--lb-app-service"

  autoscaling__enable = true
  autoscaling__replicas = local.desired-count
  autoscaling__max_replicas = local.max-count

  service__health-check = {
    resource_path = "/internal/health/ping"
    failure_threshold = 5
    healthy_threshold = 2
    interval_seconds = 20
    timeout_seconds = 10
    status_code = "200-299"
  }
}