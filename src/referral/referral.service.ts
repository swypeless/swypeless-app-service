import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ItemNotFoundException, ReferralCode, ReferralCodeProvider } from '@swypeless/core';
import { AbstractService, REFERRAL_CODE_PROVIDER } from '@swypeless/microservice-core';

/**
 * Service to manage referrals
 */
@Injectable()
export class ReferralService extends AbstractService {
  constructor(
    config: ConfigService,
    @Inject(REFERRAL_CODE_PROVIDER)
    private readonly referralCodeProvider: ReferralCodeProvider,
  ) {
    super(config);
  }

  async getReferralcode(merchant_id: string): Promise<ReferralCode> {
    const referralCode = await this.referralCodeProvider.getOne({ merchant_id });
    if (referralCode === undefined) {
      throw new ItemNotFoundException('Referral code for merchant', merchant_id);
    }
    return referralCode;
  }
}
