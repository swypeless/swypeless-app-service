import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { InternalServicesModule } from '@swypeless/microservice-core';
import { ReferralController } from './referral.controller';
import { ReferralService } from './referral.service';

@Module({
  imports: [
    ConfigModule,
    InternalServicesModule,
  ],
  providers: [
    ReferralService,
  ],
  controllers: [
    ReferralController,
  ],
  exports: [
    ReferralService,
  ],
})
export class ReferralModule {}
