import {
  BadRequestException,
  ConflictException,
  Controller,
  Get,
  HttpCode,
  NotFoundException,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  ItemNotFoundException,
  ItemNotValidException,
  OptionsNotValidException,
  ReferralCodeDTO,
  referralCodeToDTO,
  RequirementNotMetException,
} from '@swypeless/core';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { JwtAuthGuard } from '../auth';
import { MerchantId } from '../decorators';
import { ReferralService } from './referral.service';

@ApiTags('Referrals')
@ApiBearerAuth()
@Controller('referrals')
@UseGuards(JwtAuthGuard)
export class ReferralController {
  constructor(
    @InjectPinoLogger(ReferralController.name) private readonly logger: PinoLogger,
    private readonly referralService: ReferralService,
  ) { }

  @Get('code')
  @HttpCode(200)
  @ApiOperation({
    summary: 'Get referral code',
    description: 'Returns referral code for merchant.',
  })
  @ApiConsumes('application/json')
  @ApiResponse({
    status: 200,
    description: 'Referral code for merchant',
    type: ReferralCodeDTO,
  })
  @ApiResponse({
    status: 404,
    description: 'Referral code not found for merchant.',
  })
  async getReferralCode(@MerchantId() merchant_id: string): Promise<ReferralCodeDTO> {
    try {
      const referralCode = await this.referralService.getReferralcode(merchant_id);
      return referralCodeToDTO(referralCode);
    } catch (error) {
      this.logger.error('Failed to get referral code for merchant (%s): %o', merchant_id, error);
      throw this.processError(error);
    }
  }

  private processError(error: any): Error {
    switch (error.constructor) {
      case ItemNotFoundException:
        return new NotFoundException();
      case ItemNotValidException:
      case OptionsNotValidException:
        return new BadRequestException();
      case RequirementNotMetException:
        return new ConflictException();
      default:
        break;
    }
    return error;
  }
}
