import { Routes } from 'nest-router';
import { PriceModule } from './price';

export const nestedRoutes: Routes = [
  {
    path: '/products',
    children: [
      {
        path: '/:product_id',
        module: PriceModule,
      },
    ],
  },
];
