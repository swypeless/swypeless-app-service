import { ApiProperty } from '@nestjs/swagger';
import { DataListDTO, DATA_LIST_OBJECT, SaleDTO } from '@swypeless/core';

export class SaleDataListDTO extends DataListDTO<SaleDTO> {
  @ApiProperty({
    description: 'Object type',
    example: DATA_LIST_OBJECT,
  })
  object: string;

  @ApiProperty({
    description: 'Array of elements',
    type: [SaleDTO],
  })
  data: SaleDTO[];

  @ApiProperty({
    description: 'Whether or not there are more sales available after this set.',
    example: false,
  })
  has_more: boolean;
}
