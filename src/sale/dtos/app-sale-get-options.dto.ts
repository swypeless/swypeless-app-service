import { ApiProperty } from '@nestjs/swagger';
import {
  CreatableOrderedListOptionsDTO,
  SaleStatus,
  IsSaleStatus,
  IsSaleGetOptionsSearch,
  TransformBooleanString,
} from '@swypeless/core';
import { IsBoolean, IsEmpty, IsOptional } from 'class-validator';
import { AppSaleGetOptions } from '../interfaces';

export class AppSaleGetOptionsDTO
  extends CreatableOrderedListOptionsDTO
  implements AppSaleGetOptions {
  @ApiProperty({
    description: 'List of sale statuses. Usage: "/resource?statuses[]=partially_refunded&statuses=refunded',
    enum: SaleStatus,
    example: [SaleStatus.PartiallyRefunded, SaleStatus.Refunded],
    isArray: true,
    required: false,
  })
  @IsOptional()
  @IsSaleStatus({ each: true })
  statuses?: SaleStatus[];

  @ApiProperty({
    description: 'Search name, email',
    example: 'JohnSmith@gmail.com',
    required: false,
  })
  @IsOptional()
  @IsSaleGetOptionsSearch()
  search?: string;

  @ApiProperty({
    description: 'Loads Stripe client secret',
    example: true,
    required: false,
  })
  @IsOptional()
  @IsBoolean()
  @TransformBooleanString
  load_stripe_client_secret?: boolean;

  @IsEmpty()
  all?: boolean;
}
