import { ApiExtraModels, ApiProperty, getSchemaPath } from '@nestjs/swagger';
import {
  ConvenienceFeeSaleComponentSaleDTO,
  DTO,
  IsSaleAmount,
  IsSaleCurrency,
  IsSaleCustomerId,
  IsSaleCustomerNote,
  IsSaleMerchantNote,
  IsSaleMetadata,
  IsSaleReceiptEmail,
  IsSaleSubtotal,
  IsSaleType,
  Metadata,
  SaleComponentSaleDTO,
  saleComponentSaleDTODiscrimiator,
  SaleProductSaleDTO,
  SaleType,
  TaxSaleComponentSaleDTO,
  TipSaleComponentSaleDTO,
} from '@swypeless/core';
import { DiscriminableType } from '@swypeless/core/dist/utils';
import { Type } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsOptional,
  ValidateNested,
} from 'class-validator';
import { AppUpdateSaleOptions } from '../interfaces';

@ApiExtraModels(
  ConvenienceFeeSaleComponentSaleDTO,
  TaxSaleComponentSaleDTO,
  TipSaleComponentSaleDTO,
)
export class AppUpdateSaleOptionsDTO extends DTO implements AppUpdateSaleOptions {
  @ApiProperty({
    description: 'Sale amount',
    example: 1050,
    minimum: 0,
  })
  @IsSaleAmount()
  amount: number;

  @ApiProperty({
    description: 'Presentment currency of the sale',
    example: 'usd',
    minLength: 3,
    maxLength: 4,
  })
  @IsSaleCurrency()
  currency: string;

  @ApiProperty({
    description: 'Id of the customer of the sale (should be just null for now)',
    example: null,
  })
  @IsSaleCustomerId()
  customer_id: string | null;

  @ApiProperty({
    description: 'Note for the customer',
    example: 'Thank you!',
    nullable: true,
    maxLength: 500,
  })
  @IsSaleCustomerNote()
  customer_note: string | null;

  @ApiProperty({
    description: 'Note for the merchant',
    example: 'Size X-Large',
    nullable: true,
    maxLength: 500,
  })
  @IsSaleMerchantNote()
  merchant_note: string | null;

  @ApiProperty({
    description: 'Sale metadata',
    example: { key: 'value' },
  })
  @IsSaleMetadata()
  metadata: Metadata;

  @ApiProperty({
    description: 'Receipt email',
    example: 'email@swypeless.com',
    maxLength: 64,
    nullable: true,
  })
  @IsSaleReceiptEmail()
  receipt_email: string | null;

  @ApiProperty({
    description: 'Subtotal of the products in the sale',
    example: 1050,
  })
  @IsSaleSubtotal()
  subtotal: number;

  @ApiProperty({
    description: 'Sale type',
    example: SaleType.Simple,
    enum: SaleType,
  })
  @IsSaleType()
  type: SaleType;

  @ApiProperty({
    description: 'Sale product create options',
    type: [SaleProductSaleDTO],
  })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => SaleProductSaleDTO)
  products: SaleProductSaleDTO[];

  @ApiProperty({
    description: 'Sale component create options',
    anyOf: [
      {
        type: 'array',
        items: { $ref: getSchemaPath(TaxSaleComponentSaleDTO) },
      },
      {
        type: 'array',
        items: { $ref: getSchemaPath(TipSaleComponentSaleDTO) },
      },
      {
        type: 'array',
        items: { $ref: getSchemaPath(ConvenienceFeeSaleComponentSaleDTO) },
      },
    ],
    type: [TaxSaleComponentSaleDTO],
  })
  @IsArray()
  @ValidateNested({ each: true })
  @DiscriminableType(saleComponentSaleDTODiscrimiator)
  components: SaleComponentSaleDTO[];

  @ApiProperty({
    description: 'Whether to load the stripe client secret when updating',
    example: true,
    required: false,
  })
  @IsOptional()
  @IsBoolean()
  load_stripe_client_secret?: boolean;
}
