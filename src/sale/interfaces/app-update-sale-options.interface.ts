import { UpdateSaleOptions } from '@swypeless/core';

export interface AppUpdateSaleOptions extends Omit<UpdateSaleOptions, 'merchant_id'> {}
