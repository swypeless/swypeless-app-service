import { CreateSaleOptions } from '@swypeless/core';

export interface AppCreateSaleOptions extends Omit<CreateSaleOptions, 'merchant_id'> {}
