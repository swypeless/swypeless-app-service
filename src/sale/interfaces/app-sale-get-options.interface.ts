import { CreatableListOptions, SaleGetOptions } from '@swypeless/core';

export interface AppSaleGetOptions extends Pick<SaleGetOptions,
'statuses' |
'search' |
'load_stripe_client_secret'
>, Omit<CreatableListOptions, 'all'> {}
