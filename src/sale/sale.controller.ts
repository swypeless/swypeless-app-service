import {
  BadRequestException,
  Body,
  ConflictException,
  Controller,
  Get,
  HttpCode,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  CreateSaleRefundSaleOptionsDTO,
  DataListDTO,
  dataListToDTO,
  ItemNotFoundException,
  ItemNotValidException,
  OptionsNotValidException,
  RequirementNotMetException,
  SaleDTO,
  saleToDTO,
  SALE_TYPE_NAME,
} from '@swypeless/core';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { JwtAuthGuard } from '../auth';
import { DeviceId, MerchantId } from '../decorators';
import {
  SaleDataListDTO,
  AppSaleGetOptionsDTO,
  AppCreateSaleOptionsDTO,
  AppUpdateSaleOptionsDTO,
} from './dtos';
import { SaleService } from './sale.service';

@ApiTags('Sales')
@ApiBearerAuth()
@Controller('sales')
@UseGuards(JwtAuthGuard)
export class SaleController {
  constructor(
    @InjectPinoLogger(SaleController.name) private readonly logger: PinoLogger,
    private readonly saleService: SaleService,
  ) { }

  @Get()
  @HttpCode(200)
  @ApiOperation({
    summary: 'List Sales',
    description: 'Returns a list of sales for merchant',
  })
  @ApiQuery({
    description: 'List sale query options',
    type: AppSaleGetOptionsDTO,
    required: false,
  })
  @ApiResponse({
    status: 200,
    description: 'List of sales',
    type: SaleDataListDTO,
  })
  @ApiResponse({
    status: 400,
    description: 'List options not valid',
  })
  async find(
    @MerchantId() merchant_id: string,
      @Query() options: AppSaleGetOptionsDTO,
  ): Promise<DataListDTO<SaleDTO>> {
    try {
      const sales = await this.saleService.get(merchant_id, options);
      return dataListToDTO(sales, saleToDTO);
    } catch (error) {
      this.logger.error('Failed to get sale list (%s): %o %o', merchant_id, options, error);
      throw this.processError(error);
    }
  }

  @Get(':id')
  @HttpCode(200)
  @ApiOperation({
    summary: 'Get Sale',
    description: 'Returns a sale',
  })
  @ApiResponse({
    status: 200,
    description: 'Sale object',
    type: SaleDTO,
  })
  @ApiResponse({
    status: 404,
    description: 'Sale not found',
  })
  async findOne(
    @MerchantId() merchant_id: string,
      @Param('id') id: string,
  ): Promise<SaleDTO> {
    try {
      const sale = await this.saleService.getByPrimaryKey(merchant_id, id);
      if (sale === undefined) {
        throw new NotFoundException(SALE_TYPE_NAME);
      }
      return saleToDTO(sale);
    } catch (error) {
      this.logger.error('Failed to get sale (%s, %s): %o', merchant_id, id, error);
      throw this.processError(error);
    }
  }

  @Post()
  @HttpCode(201)
  @ApiOperation({
    summary: 'Create Sale',
    description: 'Creates a sale',
  })
  @ApiConsumes('application/json')
  @ApiBody({
    description: 'Provide sale options.',
    type: AppCreateSaleOptionsDTO,
    required: true,
  })
  @ApiResponse({
    status: 201,
    description: 'Sale object that was created',
    type: SaleDTO,
  })
  @ApiResponse({
    status: 400,
    description: 'Sale options not valid',
  })
  @ApiResponse({
    status: 409,
    description: 'Conflict in operation',
  })
  async create(
    @MerchantId() merchant_id: string,
      @DeviceId() device_id: string,
      @Body() options: AppCreateSaleOptionsDTO,
  ): Promise<SaleDTO> {
    try {
      const defaultMetadata = {
        device: device_id,
      };
      const newSale = await this.saleService.add(merchant_id, {
        ...options,
        metadata: {
          ...options.metadata,
          ...defaultMetadata,
        },
      });
      return saleToDTO(newSale);
    } catch (error) {
      this.logger.error('Failed to create sale (%s): %o %o', merchant_id, options, error);
      throw this.processError(error);
    }
  }

  @Put(':id')
  @HttpCode(200)
  @ApiOperation({
    summary: 'Update Sale',
    description: 'Updates a sale',
  })
  @ApiConsumes('application/json')
  @ApiBody({
    description: 'Provide sale options.',
    type: AppUpdateSaleOptionsDTO,
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Sale object that was updated',
    type: SaleDTO,
  })
  @ApiResponse({
    status: 400,
    description: 'Sale options not valid',
  })
  @ApiResponse({
    status: 404,
    description: 'Sale not found',
  })
  @ApiResponse({
    status: 409,
    description: 'Conflict in operation',
  })
  async update(
    @MerchantId() merchant_id: string,
      @Param('id') id: string,
      @Body() options: AppUpdateSaleOptionsDTO,
  ): Promise<SaleDTO> {
    try {
      const updatedSale = await this.saleService.update(merchant_id, id, options);
      return saleToDTO(updatedSale);
    } catch (error) {
      this.logger.error(
        'Failed to update sale (%s, %s): %o %o',
        merchant_id,
        id,
        options,
        error,
      );
      throw this.processError(error);
    }
  }

  @Post(':id/cancel')
  @HttpCode(200)
  @ApiOperation({
    summary: 'Cancel Sale',
    description: 'Cancels a sale',
  })
  @ApiConsumes('application/json')
  @ApiResponse({
    status: 200,
    description: 'Sale object that was canceled',
    type: SaleDTO,
  })
  @ApiResponse({
    status: 404,
    description: 'Sale not found',
  })
  @ApiResponse({
    status: 409,
    description: 'Conflict in operation',
  })
  async cancel(
    @MerchantId() merchant_id: string,
      @Param('id') id: string,
  ): Promise<SaleDTO> {
    try {
      const sale = await this.saleService.cancel(merchant_id, id);
      return saleToDTO(sale);
    } catch (error) {
      this.logger.error(
        'Failed to cancel sale (%s, %s): %o',
        merchant_id,
        id,
        error,
      );
      throw this.processError(error);
    }
  }

  @Post(':id/refunds')
  @HttpCode(201)
  @ApiOperation({
    summary: 'Create Sale Refund',
    description: 'Creates a sale refund',
  })
  @ApiConsumes('application/json')
  @ApiBody({
    description: 'Provide sale refund options.',
    type: CreateSaleRefundSaleOptionsDTO,
    required: true,
  })
  @ApiResponse({
    status: 201,
    description: 'Sale object with the created refund',
    type: SaleDTO,
  })
  @ApiResponse({
    status: 400,
    description: 'Sale refund options not valid',
  })
  @ApiResponse({
    status: 409,
    description: 'Conflict in operation',
  })
  async createRefund(
    @MerchantId() merchant_id: string,
      @Param('id') id: string,
      @Body() options: CreateSaleRefundSaleOptionsDTO,
  ): Promise<SaleDTO> {
    try {
      const newSale = await this.saleService.addRefund(merchant_id, id, options);
      return saleToDTO(newSale);
    } catch (error) {
      this.logger.error('Failed to create sale refund (%s, %s): %o %o', merchant_id, id, options, error);
      throw this.processError(error);
    }
  }

  private processError(error: any): Error {
    switch (error.constructor) {
      case ItemNotFoundException:
        return new NotFoundException();
      case ItemNotValidException:
      case OptionsNotValidException:
        return new BadRequestException();
      case RequirementNotMetException:
        return new ConflictException();
      default:
        break;
    }
    return error;
  }
}
