import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  CreateSaleRefundSaleOptions,
  DataList,
  ItemNotFoundException,
  OrderDirection,
  Sale,
  SaleProvider,
  SALE_TYPE_NAME,
} from '@swypeless/core';
import { AbstractService, SALE_PROVIDER } from '@swypeless/microservice-core';
import {
  AppCreateSaleOptions,
  AppSaleGetOptions,
  AppUpdateSaleOptions,
} from './interfaces';

/**
 * Service to manage Sales
 */
@Injectable()
export class SaleService extends AbstractService {
  constructor(
    config: ConfigService,
    @Inject(SALE_PROVIDER) private readonly saleProvider: SaleProvider,
  ) {
    super(config);
  }

  async get(merchant_id: string, options: AppSaleGetOptions): Promise<DataList<Sale>> {
    return this.saleProvider.get({
      ...options,
      merchant_id,
      order: {
        created: OrderDirection.Descending,
      },
    });
  }

  async getByPrimaryKey(merchant_id: string, sale_id: string): Promise<Sale | undefined> {
    return this.saleProvider.getOne({
      id: sale_id,
      merchant_id,
      load_stripe_client_secret: true,
    });
  }

  async add(merchant_id: string, options: AppCreateSaleOptions): Promise<Sale> {
    return this.saleProvider.add({
      merchant_id,
      ...options,
    });
  }

  async update(
    merchant_id: string,
    sale_id: string,
    options: AppUpdateSaleOptions,
  ): Promise<Sale> {
    const existingSale = await this.getByPrimaryKey(merchant_id, sale_id);
    if (existingSale === undefined) {
      throw new ItemNotFoundException(SALE_TYPE_NAME, sale_id);
    }
    return this.saleProvider.update({
      merchant_id,
      ...options,
    }, sale_id);
  }

  async cancel(merchant_id: string, sale_id: string): Promise<Sale> {
    const existingSale = await this.getByPrimaryKey(merchant_id, sale_id);
    if (existingSale === undefined) {
      throw new ItemNotFoundException(SALE_TYPE_NAME, sale_id);
    }
    return this.saleProvider.cancel(sale_id);
  }

  async addRefund(
    merchant_id: string,
    sale_id: string,
    options: CreateSaleRefundSaleOptions,
  ): Promise<Sale> {
    const existingSale = await this.getByPrimaryKey(merchant_id, sale_id);
    if (existingSale === undefined) {
      throw new ItemNotFoundException(SALE_TYPE_NAME, sale_id);
    }
    return this.saleProvider.addRefund(sale_id, options);
  }
}
