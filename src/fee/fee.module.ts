import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { InternalServicesModule } from '@swypeless/microservice-core';
import { FeeController } from './fee.controller';
import { FeeService } from './fee.service';

@Module({
  imports: [
    ConfigModule,
    InternalServicesModule,
  ],
  providers: [
    FeeService,
  ],
  controllers: [
    FeeController,
  ],
  exports: [
    FeeService,
  ],
})
export class FeeModule {}
