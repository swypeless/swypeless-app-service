import {
  BadRequestException,
  ConflictException,
  Controller,
  Get,
  HttpCode,
  NotFoundException,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  DataListDTO,
  dataListToDTO,
  FeeComponentDTO,
  feeComponentToDTO,
  ItemNotFoundException,
  OptionsNotValidException,
  RequirementNotMetException,
} from '@swypeless/core';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { JwtAuthGuard } from '../auth';
import { MerchantId } from '../decorators';
import { FeeComponentDataListDTO, FeeSummaryDTO, feeSummaryToDto } from './dtos';
import { FeeService } from './fee.service';

@ApiTags('Fees')
@ApiBearerAuth()
@Controller('fees')
@UseGuards(JwtAuthGuard)
export class FeeController {
  constructor(
    @InjectPinoLogger(FeeController.name) private readonly logger: PinoLogger,
    private readonly feeService: FeeService,
  ) { }

  @Get('components')
  @HttpCode(200)
  @ApiOperation({
    summary: 'Get Fee Components',
    description: 'Returns the fee components for merchant',
  })
  @ApiResponse({
    status: 200,
    description: 'Fee components',
    type: FeeComponentDataListDTO,
  })
  @ApiResponse({
    status: 404,
    description: 'Merchant not found',
  })
  async getCurrentComponents(
    @MerchantId() merchant_id: string,
  ): Promise<DataListDTO<FeeComponentDTO>> {
    try {
      const components = await this.feeService.getCurrentComponents(merchant_id);
      return dataListToDTO(components, feeComponentToDTO);
    } catch (error) {
      this.logger.error('Failed to get fee components (%s): %o', merchant_id, error);
      throw this.processError(error);
    }
  }

  @Get('summary')
  @HttpCode(200)
  @ApiOperation({
    summary: 'Get fee summary',
    description: 'Returns the fee summary for merchant',
  })
  @ApiResponse({
    status: 200,
    description: 'Fee summary',
    type: FeeSummaryDTO,
  })
  @ApiResponse({
    status: 404,
    description: 'Merchant not found',
  })
  async getFeeSummary(@MerchantId() merchant_id: string): Promise<FeeSummaryDTO> {
    try {
      const feeSummary = await this.feeService.getFeeSummary(merchant_id);
      return feeSummaryToDto(feeSummary);
    } catch (error) {
      this.logger.error('Failed to get fee summary (%s): %o', merchant_id, error);
      throw this.processError(error);
    }
  }

  private processError(error: any): Error {
    switch (error.constructor) {
      case ItemNotFoundException:
        return new NotFoundException(error.merchant);
      case OptionsNotValidException:
        return new BadRequestException(error.merchant);
      case RequirementNotMetException:
        return new ConflictException(error.merchant);
      default:
        break;
    }
    return error;
  }
}
