import { ApiProperty } from '@nestjs/swagger';
import { DataListDTO, DATA_LIST_OBJECT, FeeComponentDTO } from '@swypeless/core';

export class FeeComponentDataListDTO extends DataListDTO<FeeComponentDTO> {
  @ApiProperty({
    description: 'Object type',
    example: DATA_LIST_OBJECT,
  })
  object: string;

  @ApiProperty({
    description: 'Array of elements',
    type: [FeeComponentDTO],
  })
  data: FeeComponentDTO[];

  @ApiProperty({
    description: 'Whether or not there are more fee components available after this set.',
    example: false,
  })
  has_more: boolean;
}
