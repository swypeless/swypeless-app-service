import { convertType } from '@swypeless/core';
import { FeeSummary } from '../interfaces';
import { FeeSummaryDTO } from './fee-summary.dto';

export function feeSummaryToDto(feeSummary: FeeSummary): FeeSummaryDTO {
  return convertType(feeSummary, {
    outputBase: new FeeSummaryDTO(),
  });
}
