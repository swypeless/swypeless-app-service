import { ApiProperty } from '@nestjs/swagger';
import { FeeComponentDTO, ResponseDTO, TransformDate } from '@swypeless/core';
import { FeeSummary, FEE_SUMMARY_OBJECT } from '../interfaces';

export class FeeSummaryDTO extends ResponseDTO implements FeeSummary {
  @ApiProperty({
    description: 'Object type',
    example: FEE_SUMMARY_OBJECT,
  })
  object = FEE_SUMMARY_OBJECT;

  @ApiProperty({
    description: 'Base fee components',
    type: [FeeComponentDTO],
  })
  base_fee_components: FeeComponentDTO[];

  @ApiProperty({
    description: 'Incentive fee components',
    type: [FeeComponentDTO],
  })
  incentive_fee_components: FeeComponentDTO[];

  @ApiProperty({
    description: 'Date when all incentive policies expire',
    type: Number,
    example: 1626889787,
  })
  @TransformDate
  incentive_policy_expiration: Date | null;
}
