import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  DataList,
  MerchantFeePolicyProvider,
  FeeComponent,
  MerchantProvider,
  ItemNotFoundException,
  MERCHANT_TYPE_NAME,
  MerchantMetadataKeys,
  RequirementNotMetException,
  FeePolicyType,
  FeePolicyProvider,
} from '@swypeless/core';
import {
  AbstractService, FEE_POLICY_PROVIDER, MERCHANT_FEE_POLICY_PROVIDER, MERCHANT_PROVIDER,
} from '@swypeless/microservice-core';
import { FeeSummary, FEE_SUMMARY_OBJECT } from './interfaces';

/**
 * Service to manage Fees
 */
@Injectable()
export class FeeService extends AbstractService {
  constructor(
    config: ConfigService,
    @Inject(MERCHANT_PROVIDER)
    private readonly merchantProvider: MerchantProvider,
    @Inject(MERCHANT_FEE_POLICY_PROVIDER)
    private readonly merchantFeePolicyProvider: MerchantFeePolicyProvider,
    @Inject(FEE_POLICY_PROVIDER)
    private readonly feePolicyProvider: FeePolicyProvider,
  ) {
    super(config);
  }

  async getCurrentComponents(merchant_id: string): Promise<DataList<FeeComponent>> {
    return this.merchantFeePolicyProvider.getQualifiedFeeComponents({ merchant_id });
  }

  async getFeeSummary(merchant_id: string): Promise<FeeSummary> {
    // get merchant
    const merchant = await this.merchantProvider.getByPrimaryKey(merchant_id);
    if (merchant === undefined) {
      throw new ItemNotFoundException(merchant_id, MERCHANT_TYPE_NAME);
    }

    // get point in time fee policies
    const activePolicies = await this.merchantFeePolicyProvider.getPointInTimeFeePolicies(
      merchant_id,
      undefined,
      { all: true },
    );
    // filter point in time fee policies for base policies
    const activeBasePolicies = activePolicies.data
      .filter((policy) => policy.type === FeePolicyType.Base);
    // get base fee components
    const baseFeePolicyKeys = activeBasePolicies.map((policy) => policy.key);
    const baseFeeComponents = await this.merchantFeePolicyProvider.getQualifiedFeeComponents({
      merchant_id,
      fee_policy_keys: baseFeePolicyKeys,
    });

    // get merchant incentive fee policy key
    const incentiveFeePolicyKey = merchant.metadata[MerchantMetadataKeys.INCENTIVE_FEE_POLICY];
    if (incentiveFeePolicyKey === undefined) {
      throw new RequirementNotMetException(
        'Missing incentive fee policy key for merchant',
        merchant_id,
      );
    }
    // get incentive fee components
    const incentiveFeeComponents = await this.merchantFeePolicyProvider.getQualifiedFeeComponents({
      merchant_id,
      fee_policy_keys: [incentiveFeePolicyKey],
    });

    // get all merchant fee policies that haven't expired
    const merchantFeePolicyPromises = [
      // policies that will expire in the future
      this.merchantFeePolicyProvider.get({
        merchant_id,
        end_date: { gt: new Date() },
        all: true,
      }),
      // policies that won't expire
      this.merchantFeePolicyProvider.get({
        merchant_id,
        end_date: null,
        all: true,
      }),
    ];
    const merchantFeePolicies = (await Promise.all(merchantFeePolicyPromises))
      .reduce((prev, { data }) => [...prev, ...data], []);
    // get all unique fee policies
    const uniqueFeePolicyKeys = new Set(merchantFeePolicies.map((policy) => policy.policy_key));
    const feePolicies = await this.feePolicyProvider.get({
      keys: [...uniqueFeePolicyKeys],
      all: true,
    });
    // filter for incentive policies
    const incentiveFeePolicies = feePolicies.data
      .filter((policy) => policy.type === FeePolicyType.Incentive);
    // identify when incentive policies will expire
    const incentiveFeePolicyKeys = incentiveFeePolicies.map((policy) => policy.key);
    const incenitveMerchantFeePolicies = merchantFeePolicies
      .filter((policy) => incentiveFeePolicyKeys.includes(policy.policy_key));
    const incentiveFeePolicyExpiration = incenitveMerchantFeePolicies
      .reduce((prev: Date | null, { end_date }) => {
        if (prev === null) {
          return end_date;
        }
        if (end_date === null || prev > end_date) {
          return prev;
        }
        return end_date;
      },
      null);

    return {
      object: FEE_SUMMARY_OBJECT,
      base_fee_components: baseFeeComponents.data,
      incentive_fee_components: incentiveFeeComponents.data,
      incentive_policy_expiration: incentiveFeePolicyExpiration,
    };
  }
}
