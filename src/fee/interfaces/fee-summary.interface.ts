import { Base, FeeComponent } from '@swypeless/core';

export const FEE_SUMMARY_OBJECT = 'fee_summary';

export interface FeeSummaryLike {
  base_fee_components: FeeComponent[];
  incentive_fee_components: FeeComponent[];
  incentive_policy_expiration: Date | null;
}

export interface FeeSummary extends Base, FeeSummaryLike {}
