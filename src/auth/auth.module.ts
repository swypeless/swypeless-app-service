import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import {
  InternalServicesModule,
  JwtOptionsModule,
  JwtOptionsProvider,
  SecretsModule,
} from '@swypeless/microservice-core';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { BearerStrategy, JwtStrategy } from './strategies';

@Module({
  imports: [
    InternalServicesModule,
    SecretsModule,
    PassportModule,
    JwtOptionsModule,
    JwtModule.registerAsync({
      imports: [JwtOptionsModule],
      useClass: JwtOptionsProvider,
    }),
  ],
  providers: [
    AuthService,
    BearerStrategy,
    JwtStrategy,
  ],
  controllers: [AuthController],
})
export class AuthModule { }
