import {
  DeviceType,
  DeviceOSType,
} from '@swypeless/core';

export interface RequestConnectionPassportOptions {
  device_id?: string;
  ref_id?: string;
  type?: DeviceType;
  os_type?: DeviceOSType;
}

export interface ConnectionPassport {
  device_id: string;
  connection_url: string;
}
