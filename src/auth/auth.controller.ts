import {
  BadRequestException,
  Body,
  ConflictException,
  Controller,
  HttpCode,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  ItemNotFoundException,
  ItemNotValidException,
  OptionsNotValidException,
  RequirementNotMetException,
  AuthTokenDTO,
  authTokenToDTO,
  AuthToken,
  DependencyNotFoundException,
} from '@swypeless/core';
import { plainToClass } from 'class-transformer';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { DeviceId, MerchantId, RefreshedAuthToken } from '../decorators';
import { AuthService } from './auth.service';
import {
  AuthCheckDTO,
  AppAuthDisconnectedDTO,
  AppAuthTokenDTO,
  ConnectionPassportDTO,
  RequestConnectionOptionsDTO,
} from './dtos';
import { BearerAuthGuard, JwtAuthGuard } from './guards';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(
    @InjectPinoLogger(AuthController.name) private readonly logger: PinoLogger,
    private readonly authService: AuthService,
  ) {}

  @Post('connect')
  @HttpCode(201)
  @ApiOperation({
    summary: 'App Connect endpoint',
    description: 'Provides connection passport (connection information) such that an app client can connect a stripe account.',
  })
  @ApiConsumes('application/json')
  @ApiBody({
    description: 'Provide [device_id] or [ref_id, type and os_type].',
    type: RequestConnectionOptionsDTO,
    required: true,
  })
  @ApiResponse({
    status: 201,
    description: 'The connection passport information has been provided.',
    type: ConnectionPassportDTO,
  })
  @ApiResponse({
    status: 400,
    description: 'Request body not valid or device id not found.',
  })
  @ApiResponse({
    status: 403,
    description: 'Device not allowed to connect.',
  })
  async requestConnection(
    @Body() options: RequestConnectionOptionsDTO,
  ): Promise<ConnectionPassportDTO> {
    try {
      const connectionPassport = await this.authService.getConnectionPassport(options);
      return plainToClass(ConnectionPassportDTO, connectionPassport);
    } catch (error: any) {
      this.logger.error('Request connection error: %o %o', options, error);
      switch (error.constructor) {
        case ItemNotFoundException:
        case ItemNotValidException:
        case OptionsNotValidException:
          throw new BadRequestException(error.message);
        case RequirementNotMetException:
          throw new ConflictException(error.message);
        default:
          break;
      }
      throw error;
    }
  }

  @UseGuards(BearerAuthGuard)
  @Post('token')
  @HttpCode(201)
  @ApiOperation({
    summary: 'App token refresh endpoint',
    description: 'Refreshes an authorization token if valid.',
  })
  @ApiBearerAuth()
  @ApiResponse({
    status: 201,
    description: 'The refreshed auth token.',
    type: AppAuthTokenDTO,
  })
  @ApiResponse({
    status: 400,
    description: 'Could not find token or token invalid',
  })
  @ApiResponse({
    status: 403,
    description: 'Token expired or device not allowed to connect.',
  })
  async refreshToken(
    @RefreshedAuthToken() authToken: AuthToken,
  ): Promise<AuthTokenDTO> {
    try {
      return authTokenToDTO(authToken);
    } catch (error: any) {
      this.logger.error('Refresh token error: %o', error);
      switch (error.constructor) {
        case ItemNotFoundException:
        case ItemNotValidException:
        case OptionsNotValidException:
          throw new BadRequestException(error.message);
        case RequirementNotMetException:
          throw new ConflictException(error.message);
        default:
          break;
      }
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('disconnect')
  @HttpCode(201)
  @ApiOperation({
    summary: 'App Disconnect endpoint',
    description: 'Disconnects a device from the app.',
  })
  @ApiBearerAuth()
  @ApiResponse({
    status: 201,
    description: 'The device was disconnected.',
    type: AppAuthDisconnectedDTO,
  })
  @ApiResponse({
    status: 400,
    description: 'Merchant, device or connection not found.',
  })
  async disconnect(
    @MerchantId() merchant_id: string,
      @DeviceId() device_id: string,
  ): Promise<AppAuthDisconnectedDTO> {
    try {
      const authDisconnected = await this.authService.disconnect(merchant_id, device_id);
      const dto = new AppAuthDisconnectedDTO();
      dto.disconnected = authDisconnected.disconnected;
      return dto;
    } catch (error: any) {
      this.logger.error('Disconnect error (%s, %s): %o', merchant_id, device_id, error);
      switch (error.constructor) {
        case ItemNotFoundException:
        case DependencyNotFoundException:
        case OptionsNotValidException:
          throw new BadRequestException(error.message);
        default:
          break;
      }
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('check')
  @HttpCode(201)
  @ApiOperation({
    summary: 'App Auth Check endpoint',
    description: 'Checks if the access token is valid.',
  })
  @ApiBearerAuth()
  @ApiResponse({
    status: 201,
    description: 'The access token is valid.',
    type: AuthCheckDTO,
  })
  @ApiResponse({
    status: 401,
    description: 'The access token is not valid.',
  })
  async check(): Promise<AuthCheckDTO> {
    const result = new AuthCheckDTO();
    result.valid = true;
    return result;
  }
}
