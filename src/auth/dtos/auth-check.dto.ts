import { ApiProperty } from '@nestjs/swagger';
import { ResponseDTO } from '@swypeless/core';
import { IsBoolean } from 'class-validator';

const AUTH_CHECK_OBJECT = 'auth_check';

export class AuthCheckDTO extends ResponseDTO {
  @ApiProperty({
    description: 'Object type',
    example: AUTH_CHECK_OBJECT,
  })
  object = AUTH_CHECK_OBJECT;

  @ApiProperty({
    description: 'Whether access token is valid.',
    example: true,
  })
  @IsBoolean()
  valid: boolean;
}
