import { ApiProperty } from '@nestjs/swagger';
import { AuthTokenDTO, AuthTokenType, AUTH_TOKEN_OBJECT } from '@swypeless/core';

export class AppAuthTokenDTO extends AuthTokenDTO {
  @ApiProperty({
    description: 'Object type',
    example: AUTH_TOKEN_OBJECT,
  })
  object = AUTH_TOKEN_OBJECT;

  @ApiProperty({
    description: 'Api access token, jwt',
    example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6I...kJWZwjfziAj1uPWLWK6F6ckGHmgotyY',
  })
  access_token: string;

  @ApiProperty({
    description: 'Api refresh token',
    example: 'e05a9406ceb9...b916610f25c6',
  })
  refresh_token: string;

  @ApiProperty({
    description: 'Auth token type',
    enum: AuthTokenType,
    example: AuthTokenType.Bearer,
  })
  token_type: AuthTokenType;

  @ApiProperty({
    type: Number,
    description: 'Time when refresh token will expire (unix)',
    example: 1626889787,
  })
  expires: Date;
}
