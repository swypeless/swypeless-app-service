import { ApiProperty } from '@nestjs/swagger';
import { DTOPrimaryKey, ResponseDTO } from '@swypeless/core';
import { ConnectionPassport } from '../types';

@DTOPrimaryKey('device_id')
export class ConnectionPassportDTO extends ResponseDTO implements ConnectionPassport {
  @ApiProperty({
    description: 'Device id',
    example: 'dev_5xhgEJqMW7Ldcly4lVVspC',
  })
  device_id: string;

  @ApiProperty({
    description: 'Object type',
    example: 'connection_passport',
  })
  object = 'connection_passport';

  @ApiProperty({
    description: 'Connection url',
    example: 'https://connect.stripe.com/oauth/authorize?response_type=code&scope=read_write&client_id=12345&state=67890',
  })
  connection_url: string;
}
