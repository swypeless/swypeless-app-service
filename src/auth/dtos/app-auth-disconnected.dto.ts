import { ApiProperty } from '@nestjs/swagger';
import {
  AUTH_DISCONNECTED_OBJECT,
  IsAuthDisconnectedDisconnected,
  IsAuthDisconnectedObject,
  ResponseDTO,
} from '@swypeless/core';
import { AppAuthDisconnected } from '../interfaces';

export class AppAuthDisconnectedDTO extends ResponseDTO implements AppAuthDisconnected {
  @ApiProperty({
    description: 'Object type',
    example: AUTH_DISCONNECTED_OBJECT,
  })
  @IsAuthDisconnectedObject()
  object = AUTH_DISCONNECTED_OBJECT;

  @ApiProperty({
    description: 'Whether was successfully disconnected.',
    example: true,
  })
  @IsAuthDisconnectedDisconnected()
  disconnected: boolean;
}
