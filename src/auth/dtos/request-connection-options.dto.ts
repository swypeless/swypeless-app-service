import { ApiProperty } from '@nestjs/swagger';
import {
  DeviceOSType,
  DeviceType,
  DEVICE_PREFIX,
  IsDeviceExternalId,
  IsDeviceId,
  IsDeviceOSType,
  IsDeviceType,
  Regex,
  RequiresProperties,
} from '@swypeless/core';
import { IsOptional } from 'class-validator';
import { RequestConnectionPassportOptions } from '../types';

const CreateDeviceRequirements = RequiresProperties([
  'ref_id',
  'type',
  'os_type',
]);

export class RequestConnectionOptionsDTO implements RequestConnectionPassportOptions {
  @ApiProperty({
    pattern: String(Regex.makePrefixedIdRegex(DEVICE_PREFIX)),
    required: false,
    description: 'Swypeless provided device id',
  })
  @IsOptional()
  @IsDeviceId()
  device_id?: string;

  @ApiProperty({
    required: false,
    description: 'Client generated reference id, requires type and os_type if set',
    minLength: 16,
    maxLength: 64,
  })
  @IsOptional()
  @CreateDeviceRequirements
  @IsDeviceExternalId()
  ref_id?: string;

  @ApiProperty({
    enum: DeviceType,
    required: false,
    description: 'Client type, requires ref_id and os_type if set',
  })
  @IsOptional()
  @CreateDeviceRequirements
  @IsDeviceType()
  type?: DeviceType;

  @ApiProperty({
    enum: DeviceOSType,
    required: false,
    description: 'Client OS type, requires ref_id and type if set',
  })
  @IsOptional()
  @CreateDeviceRequirements
  @IsDeviceOSType()
  os_type?: DeviceOSType;
}
