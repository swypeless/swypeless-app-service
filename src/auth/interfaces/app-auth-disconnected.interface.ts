import { AuthDisconnected } from '@swypeless/core';

export type AppAuthDisconnected = Pick<AuthDisconnected,
'object' |
'disconnected'
>;
