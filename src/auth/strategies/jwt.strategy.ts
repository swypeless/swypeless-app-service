import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Inject, Injectable } from '@nestjs/common';
import { JWT_SIGNING_SECRET } from '@swypeless/microservice-core';

interface JwtPayload {
  iss: string;
  sub: string;
  aud: string;
  exp: number;
  iat: number;
}

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
  @Inject(JWT_SIGNING_SECRET) signingSecret: string,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: signingSecret,
    });
  }

  async validate(payload: JwtPayload) {
    const [merchantId, deviceId] = JSON.parse(payload.sub);
    return { merchantId, deviceId };
  }
}
