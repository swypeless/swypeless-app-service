import { Strategy } from 'passport-http-bearer';
import { PassportStrategy } from '@nestjs/passport';
import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthProvider, AuthToken } from '@swypeless/core';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { AUTH_PROVIDER } from '@swypeless/microservice-core';

@Injectable()
export class BearerStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectPinoLogger(BearerStrategy.name) private readonly logger: PinoLogger,
    @Inject(AUTH_PROVIDER) private authProvider: AuthProvider,
  ) {
    super();
  }

  async validate(token: string): Promise<AuthToken> {
    try {
      const authToken = await this.authProvider.refreshToken(token);
      return authToken;
    } catch (error) {
      this.logger.error('Auth access bearer validation failure: %s', token);
      throw new UnauthorizedException();
    }
  }
}
