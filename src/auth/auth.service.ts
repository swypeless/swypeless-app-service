import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { HealthCheckError, HealthIndicatorResult } from '@nestjs/terminus';
import {
  AuthProvider,
  AuthToken,
  deviceLikeToDevice,
  DeviceProvider,
  DeviceStatus,
  OptionsNotValidException,
  StripeSecret,
  STRIPE_CLIENT_ID_SECRET_ID,
} from '@swypeless/core';
import {
  AbstractService,
  AUTH_PROVIDER,
  DEVICE_PROVIDER,
  IncludeHealthIndicatorFunction,
  SecretsService,
} from '@swypeless/microservice-core';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { ENV_AUTH_CONNECT_REDIRECT_URI } from '../config';
import { ConnectionPassport, RequestConnectionPassportOptions } from './types';

@Injectable()
export class AuthService extends AbstractService {
  private readonly connectRedirectUri?: string;

  constructor(
    config: ConfigService,
    @InjectPinoLogger(AuthService.name) private readonly logger: PinoLogger,
    @Inject(DEVICE_PROVIDER) private readonly deviceProvider: DeviceProvider,
    @Inject(AUTH_PROVIDER) private readonly authProvider: AuthProvider,
    private readonly secretsService: SecretsService,
  ) {
    super(config);
    this.connectRedirectUri = config.get<string>(ENV_AUTH_CONNECT_REDIRECT_URI);
  }

  async getConnectionPassport(
    options: RequestConnectionPassportOptions,
  ): Promise<ConnectionPassport> {
    this.logger.info('Requesting connection passport: %o', options);
    // get device id
    let deviceId: string;
    // use device id if it exists
    if (options.device_id) {
      deviceId = options.device_id;
    } else { // next, check ref id
      if (!options.ref_id || !options.type || !options.os_type) {
        throw new OptionsNotValidException('Request Connection Options');
      }
      // check if device with external id already exists
      let device = await this.deviceProvider.getOne({
        external_id: options.ref_id,
      });
      if (device) {
        deviceId = device.id;
      } else { // create device since not exists
        device = await this.deviceProvider.add(deviceLikeToDevice({
          external_id: options.ref_id,
          type: options.type,
          os_type: options.os_type,
          // default values
          status: DeviceStatus.Active,
          os_name: null,
          os_version: null,
          manufacturer: null,
          model: null,
          product: null,
        }));
        deviceId = device.id;
      }
    }
    // get device state
    const state = await this.authProvider.getDeviceConnectionStateState(deviceId);
    const clientId = await this.getClientId();
    // static values for now, TODO get these from remote config in future
    const stripeAuthorizeUrl = 'https://connect.stripe.com/oauth/authorize';
    const responseType = 'code';
    const scope = 'read_write';
    // build url
    const params = {
      response_type: responseType,
      scope,
      client_id: clientId,
      state,
      redirect_uri: this.connectRedirectUri,
    };
    const queryString = Object.keys(params)
      .filter((key) => !!params[key])
      .map((key) => `${key}=${params[key]}`)
      .join('&');
    const connectionUrl = `${stripeAuthorizeUrl}?${queryString}`;
    return {
      device_id: deviceId,
      connection_url: connectionUrl,
    };
  }

  async refreshToken(token: string): Promise<AuthToken> {
    return this.authProvider.refreshToken(token);
  }

  async disconnect(merchant_id: string, device_id: string) {
    return this.authProvider.disconnect(merchant_id, device_id);
  }

  @IncludeHealthIndicatorFunction
  async check(): Promise<HealthIndicatorResult> {
    try {
      await this.getClientId();
    } catch (error) {
      this.logger.error('Failed to get client id: %o', error);
      const result: HealthIndicatorResult = {
        'connect:config': {
          status: 'down',
          error,
        },
      };
      throw new HealthCheckError('Failed to load Connect!', result);
    }
    return {
      'connect:config': {
        status: 'up',
      },
    };
  }

  private async getClientId(): Promise<string> {
    const clientIdSecret = await this.secretsService
      .getSecret<StripeSecret>(STRIPE_CLIENT_ID_SECRET_ID);
    return clientIdSecret.secret;
  }
}
