import { CodeRedemptionRequest } from '@swypeless/core';

export interface AppCodeRedemptionRequest extends Omit<CodeRedemptionRequest, 'merchant_id'> {}
