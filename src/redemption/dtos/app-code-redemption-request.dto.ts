import { ApiProperty } from '@nestjs/swagger';
import { DTO, IsCodeRedemptionRequestCode } from '@swypeless/core';
import { AppCodeRedemptionRequest } from '../interfaces';

export class AppCodeRedemptionRequestDTO extends DTO implements AppCodeRedemptionRequest {
  @ApiProperty({
    description: 'Redemption code',
    example: 'ABCDEF',
    minLength: 1,
    maxLength: 10,
  })
  @IsCodeRedemptionRequestCode()
  code: string;
}
