import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { CodeRedemptionProvider, CodeRedemptionResponse } from '@swypeless/core';
import { AbstractService, CODE_REDEMPTION_PROVIDER } from '@swypeless/microservice-core';

/**
 * Service to manage redemptions
 */
@Injectable()
export class RedemptionService extends AbstractService {
  constructor(
    config: ConfigService,
    @Inject(CODE_REDEMPTION_PROVIDER)
    private readonly codeRedemptionProvider: CodeRedemptionProvider,
  ) {
    super(config);
  }

  async redeem(merchant_id: string, code: string): Promise<CodeRedemptionResponse> {
    return this.codeRedemptionProvider.redeem(code, merchant_id);
  }
}
