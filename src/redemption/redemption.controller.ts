import {
  BadRequestException,
  Body,
  ConflictException,
  Controller,
  HttpCode,
  NotFoundException,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  CodeRedemptionResponseDTO,
  codeRedemptionResponseToDTO,
  ItemNotFoundException,
  ItemNotValidException,
  OptionsNotValidException,
  RequirementNotMetException,
} from '@swypeless/core';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { JwtAuthGuard } from '../auth';
import { MerchantId } from '../decorators';
import { AppCodeRedemptionRequestDTO } from './dtos';
import { RedemptionService } from './redemption.service';

@ApiTags('Redemptions')
@ApiBearerAuth()
@Controller('redemptions')
@UseGuards(JwtAuthGuard)
export class RedemptionController {
  constructor(
    @InjectPinoLogger(RedemptionController.name) private readonly logger: PinoLogger,
    private readonly redemptionService: RedemptionService,
  ) {}

  @Post('redeem')
  @HttpCode(201)
  @ApiOperation({
    summary: 'Redeem code',
    description: 'Attempts to redeem a referral or promo code',
  })
  @ApiConsumes('application/json')
  @ApiBody({
    description: 'Provide code redemption request.',
    type: AppCodeRedemptionRequestDTO,
    required: true,
  })
  @ApiResponse({
    status: 201,
    description: 'Redemption request successfully processed (though redemption omay not be successful)',
    type: CodeRedemptionResponseDTO,
  })
  @ApiResponse({
    status: 400,
    description: 'Request body not valid',
  })
  @ApiResponse({
    status: 409,
    description: 'Conflict in operation',
  })
  async redeem(
    @MerchantId() merchant_id: string,
      @Body() request: AppCodeRedemptionRequestDTO,
  ): Promise<CodeRedemptionResponseDTO> {
    const { code } = request;
    try {
      const response = await this.redemptionService.redeem(merchant_id, code);
      return codeRedemptionResponseToDTO(response);
    } catch (error) {
      this.logger.error('Failed to redeem code (%s): %s %o', merchant_id, code, error);
      throw this.processError(error);
    }
  }

  private processError(error: any): Error {
    switch (error.constructor) {
      case ItemNotFoundException:
        return new NotFoundException();
      case ItemNotValidException:
      case OptionsNotValidException:
        return new BadRequestException();
      case RequirementNotMetException:
        return new ConflictException();
      default:
        break;
    }
    return error;
  }
}
