import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { InternalServicesModule } from '@swypeless/microservice-core';
import { RedemptionController } from './redemption.controller';
import { RedemptionService } from './redemption.service';

@Module({
  imports: [
    ConfigModule,
    InternalServicesModule,
  ],
  providers: [
    RedemptionService,
  ],
  controllers: [
    RedemptionController,
  ],
  exports: [
    RedemptionService,
  ],
})
export class RedemptionModule {}
