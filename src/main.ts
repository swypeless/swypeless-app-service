import 'reflect-metadata';
import '@swypeless/microservice-core/dist/swagger';
import {
  ACCESS_LOG_IGNORE,
  createFastifyServer,
  createNestServer,
  pinoHttpOpts,
  rootLogger,
} from '@swypeless/microservice-core';
import { DocumentBuilder } from '@nestjs/swagger';
import { FastifyRequest } from 'fastify';
import { ContentTypeParserDoneFunction } from 'fastify/types/content-type-parser';
import { Logger } from 'pino';
import {
  BASE_PATH,
  PORT,
  SERVICE_ENV,
  serviceInfo,
  pinoOptions,
  versioningOptions,
} from './config';
import { AppModule } from './app.module';
import { AuthModule } from './auth';
import { AccountModule } from './account';
import { ProductModule } from './product';
import { PriceModule } from './price';
import { CurrencyModule } from './currency';
import { DeviceModule } from './device';
import { SaleModule } from './sale';
import { FeeModule } from './fee';
import { RedemptionModule } from './redemption';
import { ReferralModule } from './referral';

async function bootstrap() {
  const rootLog: Logger = rootLogger({
    name: serviceInfo.serviceName,
    pino: pinoOptions,
  });

  const logger = rootLog.child({ context: 'bootstrap' });

  const server = createFastifyServer({
    env: SERVICE_ENV,
    basePath: BASE_PATH,
    versioningOptions,
  });

  server.getInstance().addContentTypeParser(
    'text/json',
    { parseAs: 'string' }, (request: FastifyRequest, payload: string, done: ContentTypeParserDoneFunction) => {
      (request.log || rootLog).warn(
        'Received request with "content-type: text/json"! Falling back to JSON.parse(body)...',
      );
      try {
        const json = JSON.parse(payload);
        (request.log || rootLog).debug('Request with "content-type: text/json" payload. %j', json);
        done(null, json);
      } catch (err: any) {
        err.statusCode = 400;
        done(err);
      }
    },
  );

  logger.debug('Fastify server created.');

  logger.debug('Creating NestJS application...', 'bootstrap');
  const app = await createNestServer({
    nestApplicationOptions: {},
    RootModule: AppModule,
    basePath: BASE_PATH,
    env: SERVICE_ENV,
    serviceInfo,
    fastify: server,
    logger: rootLog,
    pinoHttpOptions: pinoHttpOpts(pinoOptions),
    docs: new DocumentBuilder()
      .setTitle('Swypeless App API')
      .setDescription('API for Swypeless app.')
      .addBearerAuth({
        type: 'http',
        name: 'JWT Auth',
        description: 'Authorization for all endpoints except /auth/connect and /auth/token.',
        bearerFormat: 'JWT',
      })
      .addTag('Auth')
      .addTag('Account')
      .addTag('Device')
      .addTag('Sales')
      .addTag('Currencies')
      .addTag('Fees')
      .addTag('Products')
      .addTag('Prices')
      .addTag('Referrals')
      .addTag('Redemptions'),
    docsOptions: {
      include: [
        AuthModule,
        AccountModule,
        DeviceModule,
        SaleModule,
        CurrencyModule,
        FeeModule,
        ProductModule,
        PriceModule,
        ReferralModule,
        RedemptionModule,
      ],
    },
    accessLogIgnore: ACCESS_LOG_IGNORE,
  });

  await app.listen(PORT, '0.0.0.0');
  const url = await app.getUrl();
  logger.info(
    'Server running in env "%s" at %s%s/docs',
    SERVICE_ENV,
    url,
    BASE_PATH ? `/${BASE_PATH}` : '',
  );
}

bootstrap();
