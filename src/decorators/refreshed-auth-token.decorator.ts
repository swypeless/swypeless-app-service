import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { AuthToken } from '@swypeless/core';

export const RefreshedAuthToken = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): AuthToken | undefined => {
    const request = ctx.switchToHttp().getRequest();
    return request.user;
  },
);
