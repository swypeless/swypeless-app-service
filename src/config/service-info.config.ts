import { ServiceInfo } from '@swypeless/core';
import * as packageJson from '../../package.json';

export const serviceInfo = new ServiceInfo(packageJson);
