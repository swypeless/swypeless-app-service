import { pinoBaseOptions } from '@swypeless/microservice-core';
import { LoggerOptions } from 'pino';
import { SERVICE_ENV as env } from './constants';
import { serviceInfo } from './service-info.config';

export const pinoOptions: LoggerOptions = pinoBaseOptions({ env, serviceInfo });
