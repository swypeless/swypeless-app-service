import { envName, ServiceEnv } from '@swypeless/core';
import { serviceBasePath, servicePort } from '@swypeless/microservice-core';

// env
export const ENV_AWS_REGION = 'AWS_REGION';
export const ENV_DEBUG = 'DEBUG';
export const ENV_AUTH_CONNECT_REDIRECT_URI = 'AUTH_CONNECT_REDIRECT_URI';

// general
export const BASE_PATH = serviceBasePath();
export const PORT = servicePort();
export const SERVICE_ENV: ServiceEnv = envName() || ('local' as ServiceEnv);
