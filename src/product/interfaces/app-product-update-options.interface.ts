import { ProductUpdateOptions } from '@swypeless/core';

export type AppProductUpdateOptions = Pick<ProductUpdateOptions,
'active' |
'description' |
'name'
>;
