import { ProductCreateOptions } from '@swypeless/core';
import { AppPriceCreateOptions } from '../../price';

export interface AppProductCreateOptions extends Pick<ProductCreateOptions,
'active' |
'description' |
'name'
> {
  prices?: AppPriceCreateOptions[];
}
