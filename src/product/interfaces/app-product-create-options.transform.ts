import { convertType, ProductCreateOptions } from '@swypeless/core';
import { AppPriceCreateOptions } from '../../price';
import { AppProductCreateOptions } from './app-product-create-options.interface';
import { appPriceCreateOptionsToProductPriceCreateOptions } from './app-product-price-create-options.transform';

export function appProductCreateOptionsToProductCreateOptions(
  options: AppProductCreateOptions,
): ProductCreateOptions {
  return convertType<ProductCreateOptions>(options, {
    transformKeyValues: [
      {
        key: 'prices',
        transform: (
          prices: AppPriceCreateOptions[],
        ) => prices.map(appPriceCreateOptionsToProductPriceCreateOptions),
      },
    ],
  });
}
