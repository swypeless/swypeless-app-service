import { Product } from '@swypeless/core';
import { ValidateNested } from 'class-validator';
import { AppPrice } from '../../price';

export interface AppProduct extends Pick<Product,
'id' |
'object' |
'active' |
'created' |
'description' |
'images' |
'metadata' |
'name' |
'updated'
> {
  prices: AppPrice[];
}

export const IsAppProductPrices = ValidateNested;
