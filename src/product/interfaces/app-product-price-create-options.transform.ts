import { defaultPriceValues, ProductPriceCreateOptions } from '@swypeless/core';
import { AppPriceCreateOptions } from '../../price';

export function appPriceCreateOptionsToProductPriceCreateOptions(
  options: AppPriceCreateOptions,
): ProductPriceCreateOptions {
  return {
    ...defaultPriceValues,
    ...options,
  };
}
