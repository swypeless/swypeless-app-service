import { convertType, Product } from '@swypeless/core';
import { priceToAppPrice } from '../../price';
import { AppProduct } from './app-product.interface';

export function productToAppProduct(product: Product): AppProduct {
  return convertType<AppProduct, Product>(product, {
    includeKeys: [
      'id',
      'object',
      'active',
      'created',
      'description',
      'images',
      'metadata',
      'name',
      'updated',
    ],
    addValues: {
      prices: product.prices.map(priceToAppPrice),
    },
  });
}
