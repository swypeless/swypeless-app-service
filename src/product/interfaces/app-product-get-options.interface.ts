import { ProductGetOptions, StripePaginationParams } from '@swypeless/core';

export interface AppProductGetOptions extends StripePaginationParams, Pick<ProductGetOptions,
'active' |
'created' |
'ids'
> {}
