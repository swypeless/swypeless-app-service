import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  convertDataList,
  DataList,
  DeletedProduct,
  ItemNotFoundException,
  ProductProvider,
  PRODUCT_TYPE_NAME,
} from '@swypeless/core';
import { AbstractService, PRODUCT_PROVIDER } from '@swypeless/microservice-core';
import {
  AppProduct,
  AppProductCreateOptions,
  AppProductGetOptions,
  AppProductUpdateOptions,
  productToAppProduct,
  appProductCreateOptionsToProductCreateOptions,
} from './interfaces';

/**
 * Service to manage Products
 */
@Injectable()
export class ProductService extends AbstractService {
  constructor(
    config: ConfigService,
    @Inject(PRODUCT_PROVIDER) private readonly productProvider: ProductProvider,
  ) {
    super(config);
  }

  async get(merchant_id: string, options: AppProductGetOptions): Promise<DataList<AppProduct>> {
    const products = await this.productProvider.get(merchant_id, options);
    return convertDataList(products, productToAppProduct);
  }

  async getOne(
    merchant_id: string,
    options: AppProductGetOptions,
  ): Promise<AppProduct | undefined> {
    const product = await this.productProvider.getOne(merchant_id, options);
    if (product) {
      return productToAppProduct(product);
    }
    return undefined;
  }

  async getByPrimaryKey(merchant_id: string, product_id: string): Promise<AppProduct | undefined> {
    const product = await this.productProvider.getByPrimaryKey(merchant_id, product_id);
    if (product) {
      return productToAppProduct(product);
    }
    return undefined;
  }

  async add(merchant_id: string, options: AppProductCreateOptions): Promise<AppProduct> {
    const newProduct = await this.productProvider.add(
      merchant_id,
      appProductCreateOptionsToProductCreateOptions(options),
    );
    return productToAppProduct(newProduct);
  }

  async update(
    merchant_id: string,
    product_id: string,
    options: AppProductUpdateOptions,
  ): Promise<AppProduct> {
    const updatedProduct = await this.productProvider.update(merchant_id, product_id, options);
    return productToAppProduct(updatedProduct);
  }

  async delete(merchant_id: string, product_id: string): Promise<DeletedProduct> {
    const product = await this.getByPrimaryKey(merchant_id, product_id);
    if (product === undefined) {
      throw new ItemNotFoundException(PRODUCT_TYPE_NAME, product_id);
    }
    return this.productProvider.delete(merchant_id, product_id);
  }
}
