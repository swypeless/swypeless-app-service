import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { InternalServicesModule } from '@swypeless/microservice-core';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';

@Module({
  imports: [
    ConfigModule,
    InternalServicesModule,
  ],
  providers: [
    ProductService,
  ],
  controllers: [
    ProductController,
  ],
  exports: [
    ProductService,
  ],
})
export class ProductModule {}
