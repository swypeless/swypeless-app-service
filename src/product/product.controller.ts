import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  DataListDTO,
  dataListToDTO,
  DeletedProductDTO,
  deletedProductToDTO,
  ItemNotFoundException,
  OptionsNotValidException,
  PRODUCT_TYPE_NAME,
} from '@swypeless/core';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { JwtAuthGuard } from '../auth';
import { MerchantId } from '../decorators';
import {
  AppProductDataListDTO,
  AppProductDTO,
  AppProductGetOptionsDTO,
  appProductToDto,
  CreateAppProductDTO,
  DeletedAppProductDTO,
  UpdateAppProductDTO,
} from './dtos';
import { ProductService } from './product.service';

@ApiTags('Products')
@ApiBearerAuth()
@Controller('products')
@UseGuards(JwtAuthGuard)
export class ProductController {
  constructor(
    @InjectPinoLogger(ProductController.name) private readonly logger: PinoLogger,
    private readonly productService: ProductService,
  ) {}

  @Get()
  @HttpCode(200)
  @ApiOperation({
    summary: 'List Products',
    description: 'Returns a list of products for merchant',
  })
  @ApiResponse({
    status: 200,
    description: 'List of products',
    type: AppProductDataListDTO,
  })
  @ApiResponse({
    status: 400,
    description: 'List options not valid',
  })
  async find(
    @MerchantId() merchant_id: string,
      @Query() options: AppProductGetOptionsDTO,
  ): Promise<DataListDTO<AppProductDTO>> {
    try {
      const products = await this.productService.get(merchant_id, options);
      return dataListToDTO(products, appProductToDto);
    } catch (error) {
      this.logger.error('Failed to get product list (%s): %o %o', merchant_id, options, error);
      throw this.processError(error);
    }
  }

  @Get(':id')
  @HttpCode(200)
  @ApiOperation({
    summary: 'Get Product',
    description: 'Returns a product',
  })
  @ApiResponse({
    status: 200,
    description: 'Product object',
    type: AppProductDTO,
  })
  @ApiResponse({
    status: 404,
    description: 'Product not found',
  })
  async findOne(
    @MerchantId() merchant_id: string,
      @Param('id') id: string,
  ): Promise<AppProductDTO> {
    try {
      const product = await this.productService.getByPrimaryKey(merchant_id, id);
      if (product === undefined) {
        throw new NotFoundException(PRODUCT_TYPE_NAME);
      }
      return appProductToDto(product);
    } catch (error) {
      this.logger.error('Failed to get product (%s, %s): %o', merchant_id, id, error);
      throw this.processError(error);
    }
  }

  @Post()
  @HttpCode(201)
  @ApiOperation({
    summary: 'Create Product',
    description: 'Creates a product',
  })
  @ApiConsumes('application/json')
  @ApiBody({
    description: 'Provide product options.',
    type: CreateAppProductDTO,
    required: true,
  })
  @ApiResponse({
    status: 201,
    description: 'Product object that was created',
    type: AppProductDTO,
  })
  @ApiResponse({
    status: 400,
    description: 'Product options not valid',
  })
  async create(
    @MerchantId() merchant_id: string,
      @Body() product: CreateAppProductDTO,
  ): Promise<AppProductDTO> {
    try {
      const newProduct = await this.productService.add(merchant_id, product);
      return appProductToDto(newProduct);
    } catch (error) {
      this.logger.error('Failed to create product (%s): %o %o', merchant_id, product, error);
      throw this.processError(error);
    }
  }

  @Put(':id')
  @HttpCode(200)
  @ApiOperation({
    summary: 'Update Product',
    description: 'Updates a product',
  })
  @ApiConsumes('application/json')
  @ApiBody({
    description: 'Provide product options.',
    type: UpdateAppProductDTO,
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Product object that was updated',
    type: AppProductDTO,
  })
  @ApiResponse({
    status: 400,
    description: 'Product options not valid',
  })
  @ApiResponse({
    status: 404,
    description: 'Product not found',
  })
  async update(
    @MerchantId() merchant_id: string,
      @Param('id') id: string,
      @Body() product: UpdateAppProductDTO,
  ): Promise<AppProductDTO> {
    try {
      const newProduct = await this.productService.update(merchant_id, id, product);
      return appProductToDto(newProduct);
    } catch (error) {
      this.logger.error(
        'Failed to update product (%s, %s): %o %o',
        merchant_id,
        id,
        product,
        error,
      );
      throw this.processError(error);
    }
  }

  @Delete(':id')
  @HttpCode(200)
  @ApiOperation({
    summary: 'Delete Product',
    description: 'Deletes a product',
  })
  @ApiResponse({
    status: 200,
    description: 'Deleted product object',
    type: DeletedAppProductDTO,
  })
  @ApiResponse({
    status: 404,
    description: 'Product not found',
  })
  async remove(
    @MerchantId() merchant_id: string,
      @Param('id') id: string,
  ): Promise<DeletedProductDTO> {
    try {
      const deletedProduct = await this.productService.delete(merchant_id, id);
      return deletedProductToDTO(deletedProduct);
    } catch (error) {
      this.logger.error('Failed to delete product (%s, %s): %o', merchant_id, id, error);
      throw this.processError(error);
    }
  }

  private processError(error: Error): Error {
    switch (error.constructor) {
      case ItemNotFoundException:
        return new NotFoundException();
      case OptionsNotValidException:
        return new BadRequestException();
      default:
        break;
    }
    return error;
  }
}
