import { ApiProperty } from '@nestjs/swagger';
import {
  DTO,
  IsProductActive,
  IsProductDescription,
  IsProductName,
} from '@swypeless/core';
import { Type } from 'class-transformer';
import { IsOptional, ValidateNested } from 'class-validator';
import { CreateAppPriceDTO } from '../../price';
import { AppProductCreateOptions } from '../interfaces';

export class CreateAppProductDTO extends DTO implements AppProductCreateOptions {
  @ApiProperty({
    required: false,
    description: 'Whether the product is currently available for purchase.',
    example: true,
    default: true,
  })
  @IsOptional()
  @IsProductActive()
  active?: boolean;

  @ApiProperty({
    required: false,
    description: 'The product\'s description.',
    minLength: 1,
    maxLength: 256,
    example: 'This product is amazing',
  })
  @IsOptional()
  @IsProductDescription()
  description?: string;

  @ApiProperty({
    description: 'The product\'s name.',
    minLength: 1,
    maxLength: 64,
    example: 'Amazing product',
  })
  @IsProductName()
  name: string;

  @ApiProperty({
    required: false,
    description: 'Prices to create with the product',
    type: [CreateAppPriceDTO],
  })
  @IsOptional()
  @Type(() => CreateAppPriceDTO)
  @ValidateNested({
    each: true,
  })
  prices?: CreateAppPriceDTO[];
}
