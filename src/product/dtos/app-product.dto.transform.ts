import { plainToClass } from 'class-transformer';
import { AppProduct } from '../interfaces';
import { AppProductDTO } from './app-product.dto';

export function appProductToDto(appProduct: AppProduct): AppProductDTO {
  return plainToClass(AppProductDTO, appProduct);
}
