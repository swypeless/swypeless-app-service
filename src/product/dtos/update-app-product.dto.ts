import { ApiProperty } from '@nestjs/swagger';
import {
  DTO,
  IsProductActive,
  IsProductDescription,
  IsProductName,
} from '@swypeless/core';
import { IsOptional } from 'class-validator';
import { AppProductUpdateOptions } from '../interfaces';

export class UpdateAppProductDTO extends DTO implements AppProductUpdateOptions {
  @ApiProperty({
    required: false,
    description: 'Whether the product is currently available for purchase.',
    example: true,
    default: true,
  })
  @IsOptional()
  @IsProductActive()
  active?: boolean;

  @ApiProperty({
    required: false,
    description: 'The product\'s description.',
    minLength: 1,
    maxLength: 256,
    example: 'This product is amazing',
  })
  @IsOptional()
  @IsProductDescription()
  description?: string;

  @ApiProperty({
    description: 'The product\'s name.',
    minLength: 1,
    maxLength: 64,
    example: 'Amazing product',
  })
  @IsProductName()
  name: string;
}
