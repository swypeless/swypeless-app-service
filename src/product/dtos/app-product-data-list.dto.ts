import { ApiProperty } from '@nestjs/swagger';
import { DataListDTO, DATA_LIST_OBJECT } from '@swypeless/core';
import { AppProductDTO } from './app-product.dto';

export class AppProductDataListDTO extends DataListDTO<AppProductDTO> {
  @ApiProperty({
    description: 'Object type',
    example: DATA_LIST_OBJECT,
  })
  object: string;

  @ApiProperty({
    description: 'Array of elements',
    type: [AppProductDTO],
  })
  data: AppProductDTO[];

  @ApiProperty({
    description: 'Whether or not there are more products available after this set.',
    example: false,
  })
  has_more: boolean;
}
