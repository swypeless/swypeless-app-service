import { ApiProperty } from '@nestjs/swagger';
import { DeletedProductDTO, PRODUCT_OBJECT } from '@swypeless/core';

export class DeletedAppProductDTO extends DeletedProductDTO {
  @ApiProperty({
    description: 'Product id',
    example: 'prod_IcPmfuh1Ye6jGl',
  })
  id: string;

  @ApiProperty({
    description: 'Object type',
    example: PRODUCT_OBJECT,
  })
  object: string;

  @ApiProperty({
    description: 'Whether the product was deleted',
    example: true,
  })
  deleted: boolean;
}
