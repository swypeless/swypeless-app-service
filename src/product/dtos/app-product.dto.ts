import { ApiProperty } from '@nestjs/swagger';
import {
  DTOPrimaryKey,
  IsProductActive,
  IsProductCreated,
  IsProductDescription,
  IsProductId,
  IsProductImages,
  IsProductName,
  IsProductObject,
  IsProductUpdated,
  IsStripeMetadata,
  PRODUCT_OBJECT,
  PRODUCT_PRIMARY_KEY,
  ResponseDTO,
  StripeMetadata,
} from '@swypeless/core';
import { Type } from 'class-transformer';
import { AppPriceDTO } from '../../price';
import { AppProduct, IsAppProductPrices } from '../interfaces';

@DTOPrimaryKey(PRODUCT_PRIMARY_KEY)
export class AppProductDTO extends ResponseDTO implements AppProduct {
  @ApiProperty({
    description: 'Product id',
    example: 'prod_IcPmfuh1Ye6jGl',
  })
  @IsProductId()
  id: string;

  @ApiProperty({
    description: 'Object type',
    example: PRODUCT_OBJECT,
  })
  @IsProductObject()
  object: string;

  @ApiProperty({
    description: 'Product is active',
    example: true,
  })
  @IsProductActive()
  active: boolean;

  @ApiProperty({
    description: 'Time  product was created (unix)',
    example: 1626889787,
  })
  @IsProductCreated()
  created: number;

  @ApiProperty({
    description: 'Product description',
    example: 'This product is amazing',
  })
  @IsProductDescription()
  description: string | null;

  @ApiProperty({
    description: 'Product image list (urls)',
    example: [],
  })
  @IsProductImages()
  images: string[];

  @ApiProperty({
    description: 'Product metadata',
    example: { property: 'value' },
  })
  @IsStripeMetadata()
  metadata: StripeMetadata;

  @ApiProperty({
    description: 'Product name',
    example: 'Amazing product',
  })
  @IsProductName()
  name: string;

  @ApiProperty({
    description: 'Product price list',
    type: [AppPriceDTO],
  })
  @IsAppProductPrices()
  @Type(() => AppPriceDTO)
  prices: AppPriceDTO[];

  @ApiProperty({
    description: 'Time product was updated (unix)',
    example: 1626889787,
  })
  @IsProductUpdated()
  updated: number;
}
