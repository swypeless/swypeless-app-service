import { ApiProperty } from '@nestjs/swagger';
import {
  DTO,
  IsProductActive,
  IsProductId,
  IsStripeRangeQueryParam,
  StripeRangeQueryParam,
  TransformBooleanString,
  TransformNumberString,
} from '@swypeless/core';
import {
  IsInt,
  IsOptional,
  Max,
  Min,
} from 'class-validator';
import { AppProductGetOptions } from '../interfaces';

export class AppProductGetOptionsDTO extends DTO implements AppProductGetOptions {
  @ApiProperty({
    required: false,
    description: 'Product is active',
    example: true,
  })
  @IsOptional()
  @IsProductActive()
  @TransformBooleanString
  active?: boolean;

  @ApiProperty({
    required: false,
    description: 'Created date query param. Can specify created.lt, created.lte, created.gt, and/or created.gte as a range query instead.',
    type: 'integer',
    example: 1626889787,
  })
  @IsOptional()
  @IsStripeRangeQueryParam()
  created?: number | StripeRangeQueryParam;

  @ApiProperty({
    required: false,
    description: 'Product id to end before when listing',
    example: 'prod_IcPmfuh1Ye6jGl',
  })
  @IsOptional()
  @IsProductId()
  ending_before?: string;

  @ApiProperty({
    required: false,
    description: 'List of ids to include in list',
    example: ['prod_IcPmfuh1Ye6jGl', 'prod_1234567890abcd'],
  })
  @IsOptional()
  @IsProductId({
    each: true,
  })
  ids?: string[];

  @ApiProperty({
    required: false,
    description: 'Limit of list',
    type: 'integer',
    default: 10,
    example: 20,
    minimum: 1,
    maximum: 100,
  })
  @IsOptional()
  @IsInt()
  @Min(1)
  @Max(100)
  @TransformNumberString
  limit?: number;

  @ApiProperty({
    required: false,
    description: 'Product id to start after when listing',
    example: 'prod_IcPmfuh1Ye6jGl',
  })
  @IsOptional()
  @IsProductId()
  starting_after?: string;
}
