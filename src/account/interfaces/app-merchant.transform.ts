import { convertType, Merchant } from '@swypeless/core';
import { AppMerchant } from './app-merchant.interface';

export function merchantToAppMerchant(merchant: Merchant): AppMerchant {
  return convertType<AppMerchant, Merchant>(merchant, {
    includeKeys: [
      'id',
      'object',
      'business_profile',
      'capabilities',
      'country',
      'created',
      'default_currency',
      'display_name',
      'email',
      'individual',
      'metadata',
      'modified',
    ],
  });
}
