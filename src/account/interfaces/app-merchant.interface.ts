import { Merchant } from '@swypeless/core';

export type AppMerchant = Pick<Merchant,
'id' |
'object' |
'business_profile' |
'capabilities' |
'country' |
'created' |
'default_currency' |
'display_name' |
'email' |
'individual' |
'metadata' |
'modified'
>;
