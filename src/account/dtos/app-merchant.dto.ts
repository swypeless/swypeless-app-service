import { ApiProperty } from '@nestjs/swagger';
import {
  DTOPrimaryKey,
  IsCreated,
  IsMerchantCountry,
  IsMerchantDefaultCurrency,
  IsMerchantDisplayName,
  IsMerchantEmail,
  IsMerchantId,
  IsMerchantMetadata,
  IsMerchantObject,
  IsModified,
  MerchantBusinessProfileDTO,
  MerchantCapabilitiesDTO,
  MERCHANT_OBJECT,
  MERCHANT_PRIMARY_KEY,
  Metadata,
  PersonDTO,
  ResponseDTO,
  TransformDate,
} from '@swypeless/core';
import { ValidateNested } from 'class-validator';
import { AppMerchant } from '../interfaces';

@DTOPrimaryKey(MERCHANT_PRIMARY_KEY)
export class AppMerchantDTO extends ResponseDTO implements AppMerchant {
  @ApiProperty({
    description: 'Merchant id',
    example: 'mch_5xhgEJqMW7Ldcly4lVVspC',
  })
  @IsMerchantId()
  id: string;

  @ApiProperty({
    description: 'Object type',
    example: MERCHANT_OBJECT,
  })
  @IsMerchantObject()
  object: string;

  @ApiProperty({
    description: 'Merchant business profile',
    type: MerchantBusinessProfileDTO,
  })
  @ValidateNested()
  business_profile: MerchantBusinessProfileDTO;

  @ApiProperty({
    description: 'Merchant capabilities',
    type: MerchantCapabilitiesDTO,
  })
  @ValidateNested()
  capabilities: MerchantCapabilitiesDTO;

  @ApiProperty({
    description: 'Merchant country',
    example: 'US',
    minLength: 2,
    maxLength: 3,
  })
  @IsMerchantCountry()
  country: string;

  @ApiProperty({
    type: Number,
    description: 'Time merchant was created (unix)',
    example: 1626889787,
  })
  @IsCreated()
  @TransformDate
  created: Date;

  @ApiProperty({
    description: 'Merchant default currency',
    example: 'usd',
    minLength: 3,
    maxLength: 4,
  })
  @IsMerchantDefaultCurrency()
  default_currency: string;

  @ApiProperty({
    description: 'Merchant display name',
    example: 'Swypeless',
    maxLength: 64,
    nullable: true,
  })
  @IsMerchantDisplayName()
  display_name: string | null;

  @ApiProperty({
    description: 'Email address, must be a vaild email',
    example: 'email@swypeless.com',
    maxLength: 64,
  })
  @IsMerchantEmail()
  email: string;

  @ApiProperty({
    description: 'Merchant individual',
    type: PersonDTO,
  })
  @ValidateNested()
  individual: PersonDTO;

  @ApiProperty({
    description: 'Merchant metadata',
    example: { key: 'value' },
  })
  @IsMerchantMetadata()
  metadata: Metadata;

  @ApiProperty({
    type: Number,
    description: 'Time merchant was modified (unix)',
    example: 1626889787,
  })
  @IsModified()
  @TransformDate
  modified: Date;
}
