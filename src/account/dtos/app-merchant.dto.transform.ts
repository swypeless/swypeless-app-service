import { convertType } from '@swypeless/core';
import { AppMerchant } from '../interfaces';
import { AppMerchantDTO } from './app-merchant.dto';

export function appMerchantToDto(appMerchant: AppMerchant): AppMerchantDTO {
  return convertType<AppMerchantDTO, AppMerchant>(appMerchant, {
    outputBase: new AppMerchantDTO(),
  });
}
