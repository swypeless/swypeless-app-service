import {
  Controller,
  Get,
  HttpCode,
  NotFoundException,
  UseGuards,
} from '@nestjs/common';
import {
  BalanceDTO,
  balanceToDTO,
  ItemNotFoundException,
  MERCHANT_TYPE_NAME,
} from '@swypeless/core';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { AccountService } from './account.service';
import { JwtAuthGuard } from '../auth';
import { MerchantId } from '../decorators';
import { AppMerchantDTO, appMerchantToDto } from './dtos';

@ApiTags('Account')
@ApiBearerAuth()
@Controller('account')
@UseGuards(JwtAuthGuard)
export class AccountController {
  constructor(
    @InjectPinoLogger(AccountController.name) private readonly logger: PinoLogger,
    private readonly accountService: AccountService,
  ) { }

  @Get()
  @HttpCode(200)
  @ApiOperation({
    summary: 'Account endpoint',
    description: 'Returns the merchant object of the connected merchant.',
  })
  @ApiResponse({
    status: 200,
    description: 'Returns the merchant object of the connected merchant',
    type: AppMerchantDTO,
  })
  @ApiResponse({
    status: 404,
    description: 'Merchant not found.',
  })
  async find(@MerchantId() merchant_id: string): Promise<AppMerchantDTO> {
    try {
      const merchant = await this.accountService.get(merchant_id);
      return appMerchantToDto(merchant);
    } catch (error: any) {
      this.logger.error('Account get merchant (%s): %o', merchant_id, error);
      switch (error.constructor) {
        case ItemNotFoundException:
          throw new NotFoundException(MERCHANT_TYPE_NAME);
        default:
          break;
      }
      throw error;
    }
  }

  @Get('balance')
  @HttpCode(200)
  @ApiOperation({
    summary: 'Account balance endpoint',
    description: 'Returns the balance object of the connected merchant',
  })
  @ApiResponse({
    status: 200,
    description: 'Balance object of the connected merchant',
    type: BalanceDTO,
  })
  @ApiResponse({
    status: 404,
    description: 'Merchant not found.',
  })
  async getBalance(@MerchantId() merchant_id: string): Promise<BalanceDTO> {
    try {
      const balance = await this.accountService.getBalance(merchant_id);
      return balanceToDTO(balance);
    } catch (error: any) {
      this.logger.error('Account get merchant balance (%s): %o', merchant_id, error);
      switch (error.constructor) {
        case ItemNotFoundException:
          throw new NotFoundException(MERCHANT_TYPE_NAME);
        default:
          break;
      }
      throw error;
    }
  }
}
