import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  Balance,
  ItemNotFoundException,
  MerchantProvider,
  MERCHANT_TYPE_NAME,
} from '@swypeless/core';
import { AbstractService, MERCHANT_PROVIDER } from '@swypeless/microservice-core';
import { AppMerchant, merchantToAppMerchant } from './interfaces';

/**
 * Service to manage account
 */
@Injectable()
export class AccountService extends AbstractService {
  constructor(
    config: ConfigService,
    @Inject(MERCHANT_PROVIDER) private readonly merchantProvider: MerchantProvider,
  ) {
    super(config);
  }

  async get(merchant_id: string): Promise<AppMerchant> {
    const merchant = await this.merchantProvider.getByPrimaryKey(merchant_id);
    if (merchant) {
      return merchantToAppMerchant(merchant);
    }
    throw new ItemNotFoundException(MERCHANT_TYPE_NAME, merchant_id);
  }

  async getBalance(merchant_id: string): Promise<Balance> {
    return this.merchantProvider.getBalance(merchant_id);
  }
}
