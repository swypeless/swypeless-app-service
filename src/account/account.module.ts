import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { InternalServicesModule } from '@swypeless/microservice-core';
import { AccountController } from './account.controller';
import { AccountService } from './account.service';

@Module({
  imports: [
    ConfigModule,
    InternalServicesModule,
  ],
  providers: [
    AccountService,
  ],
  controllers: [
    AccountController,
  ],
  exports: [
    AccountService,
  ],
})
export class AccountModule { }
