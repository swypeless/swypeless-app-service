import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  NotFoundException,
  Put,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ItemNotFoundException, OptionsNotValidException } from '@swypeless/core';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { JwtAuthGuard } from '../auth';
import { DeviceId } from '../decorators';
import { DeviceService } from './device.service';
import { AppDeviceDTO, appDeviceToDto, UpdateAppDeviceDTO } from './dtos';

@ApiTags('Device')
@ApiBearerAuth()
@Controller('device')
@UseGuards(JwtAuthGuard)
export class DeviceController {
  constructor(
    @InjectPinoLogger(DeviceController.name) private readonly logger: PinoLogger,
    private readonly deviceService: DeviceService,
  ) {}

  @Get()
  @HttpCode(200)
  @ApiOperation({
    summary: 'Get Device Information',
    description: 'Returns the current device\'s information',
  })
  @ApiResponse({
    status: 200,
    description: 'Device information',
    type: AppDeviceDTO,
  })
  @ApiResponse({
    status: 404,
    description: 'Device not found',
  })
  async get(@DeviceId() device_id: string): Promise<AppDeviceDTO> {
    try {
      const appDevice = await this.deviceService.get(device_id);
      return appDeviceToDto(appDevice);
    } catch (error) {
      this.logger.error('Failed to get device info (%s): %o', device_id, error);
      throw this.processError(error);
    }
  }

  @Put()
  @HttpCode(200)
  @ApiOperation({
    summary: 'Update Device Information',
    description: 'Updates the current device',
  })
  @ApiConsumes('application/json')
  @ApiBody({
    description: 'Provide device options.',
    type: UpdateAppDeviceDTO,
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Updated device information',
    type: AppDeviceDTO,
  })
  @ApiResponse({
    status: 400,
    description: 'Device options not valid',
  })
  @ApiResponse({
    status: 404,
    description: 'Device not found',
  })
  async update(
    @DeviceId() device_id: string,
      @Body() options: UpdateAppDeviceDTO,
  ): Promise<AppDeviceDTO> {
    try {
      const device = await this.deviceService.update(device_id, options);
      return appDeviceToDto(device);
    } catch (error) {
      this.logger.error('Failed to update price (%s): %o %o', device_id, options, error);
      throw this.processError(error);
    }
  }

  private processError(error: Error): Error {
    switch (error.constructor) {
      case ItemNotFoundException:
        return new NotFoundException();
      case OptionsNotValidException:
        return new BadRequestException();
      default:
        break;
    }
    return error;
  }
}
