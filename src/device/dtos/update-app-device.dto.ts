import { ApiProperty } from '@nestjs/swagger';
import {
  DeviceOSType,
  DeviceType,
  DTO,
  IsDeviceManufacturer,
  IsDeviceModel,
  IsDeviceOSName,
  IsDeviceOSType,
  IsDeviceOSVersion,
  IsDeviceProduct,
  IsDeviceType,
} from '@swypeless/core';
import { AppDeviceUpdateOptions } from '../interfaces';

export class UpdateAppDeviceDTO extends DTO implements AppDeviceUpdateOptions {
  @ApiProperty({
    description: 'Device manufacturer',
    example: 'Apple',
    minLength: 1,
    maxLength: 64,
    nullable: true,
  })
  @IsDeviceManufacturer()
  manufacturer: string | null;

  @ApiProperty({
    description: 'Device model',
    example: 'iPhone 13,2',
    minLength: 1,
    maxLength: 64,
    nullable: true,
  })
  @IsDeviceModel()
  model: string | null;

  @ApiProperty({
    description: 'Device os type',
    example: DeviceOSType.IOS,
    enum: DeviceOSType,
  })
  @IsDeviceOSType()
  os_type: DeviceOSType;

  @ApiProperty({
    description: 'Device os name',
    example: 'iOS 14',
    minLength: 1,
    maxLength: 32,
    nullable: true,
  })
  @IsDeviceOSName()
  os_name: string | null;

  @ApiProperty({
    description: 'Device os version',
    example: '14.0.0',
    minLength: 1,
    maxLength: 32,
    nullable: true,
  })
  @IsDeviceOSVersion()
  os_version: string | null;

  @ApiProperty({
    description: 'Device product',
    example: 'iPhone 12',
    minLength: 1,
    maxLength: 64,
    nullable: true,
  })
  @IsDeviceProduct()
  product: string | null;

  @ApiProperty({
    description: 'Device type',
    example: DeviceType.Phone,
    enum: DeviceType,
    nullable: true,
  })
  @IsDeviceType()
  type: DeviceType | null;
}
