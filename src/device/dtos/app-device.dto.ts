import { ApiProperty } from '@nestjs/swagger';
import {
  DeviceOSType,
  DeviceType,
  DEVICE_OBJECT,
  DEVICE_PRIMARY_KEY,
  DTOPrimaryKey,
  IsCreated,
  IsDeviceId,
  IsDeviceManufacturer,
  IsDeviceModel,
  IsDeviceOSName,
  IsDeviceOSType,
  IsDeviceOSVersion,
  IsDeviceProduct,
  IsDeviceType,
  IsModified,
  IsObjectType,
  ResponseDTO,
  TransformDate,
} from '@swypeless/core';
import { AppDevice, IsDeviceRefId } from '../interfaces';

@DTOPrimaryKey(DEVICE_PRIMARY_KEY)
export class AppDeviceDTO extends ResponseDTO implements AppDevice {
  @ApiProperty({
    description: 'Device id',
    example: 'dev_5xhgEJqMW7Ldcly4lVVspC',
  })
  @IsDeviceId()
  id: string;

  @ApiProperty({
    description: 'Object type',
    example: DEVICE_OBJECT,
  })
  @IsObjectType(DEVICE_OBJECT)
  object: string;

  @ApiProperty({
    type: Number,
    description: 'Time device was created (unix)',
    example: 1626889787,
  })
  @IsCreated()
  @TransformDate
  created: Date;

  @ApiProperty({
    description: 'Device manufacturer',
    example: 'Apple',
    nullable: true,
  })
  @IsDeviceManufacturer()
  manufacturer: string | null;

  @ApiProperty({
    description: 'Device model',
    example: 'iPhone 13,2',
    nullable: true,
  })
  @IsDeviceModel()
  model: string | null;

  @ApiProperty({
    type: Number,
    description: 'Time device was modified (unix)',
    example: 1626889787,
  })
  @IsModified()
  @TransformDate
  modified: Date;

  @ApiProperty({
    description: 'Device os name',
    example: 'iOS 14',
    nullable: true,
  })
  @IsDeviceOSName()
  os_name: string | null;

  @ApiProperty({
    description: 'Device os type',
    example: DeviceOSType.IOS,
  })
  @IsDeviceOSType()
  os_type: DeviceOSType;

  @ApiProperty({
    description: 'Device os version',
    example: '14.0.0',
    nullable: true,
  })
  @IsDeviceOSVersion()
  os_version: string | null;

  @ApiProperty({
    description: 'Device product',
    example: 'iPhone 12',
    nullable: true,
  })
  @IsDeviceProduct()
  product: string | null;

  @ApiProperty({
    description: 'Device reference id',
    example: '58a57632-01fb-11ec-9a03-0242ac130003',
  })
  @IsDeviceRefId()
  ref_id: string;

  @ApiProperty({
    description: 'Device type',
    example: DeviceType.Phone,
    nullable: true,
  })
  @IsDeviceType()
  type: DeviceType | null;
}
