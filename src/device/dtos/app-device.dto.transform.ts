import { convertType } from '@swypeless/core';
import { AppDevice } from '../interfaces';
import { AppDeviceDTO } from './app-device.dto';

export function appDeviceToDto(appDevice: AppDevice): AppDeviceDTO {
  return convertType<AppDeviceDTO, AppDevice>(appDevice, {
    outputBase: new AppDeviceDTO(),
  });
}
