import { AppDevice } from './app-device.interface';

export type AppDeviceUpdateOptions = Pick<AppDevice,
'type' |
'os_type' |
'os_name' |
'os_version' |
'manufacturer' |
'model' |
'product'
>;
