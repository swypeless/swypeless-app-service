import { convertType, DeviceLike, DeviceStatus } from '@swypeless/core';
import { AppDeviceUpdateOptions } from './app-device-update-options.interface';

export function appDeviceUpdateOptionsToDeviceLike(
  options: AppDeviceUpdateOptions,
  external_id: string,
  status: DeviceStatus,
): DeviceLike {
  return convertType<DeviceLike, AppDeviceUpdateOptions>(options, {
    addValues: {
      status,
      external_id,
    },
  });
}
