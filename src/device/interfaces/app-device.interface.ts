import { Device, IsDeviceExternalId } from '@swypeless/core';

export interface AppDevice extends Pick<Device,
'id' |
'object' |
'type' |
'os_type' |
'os_name' |
'os_version' |
'manufacturer' |
'model' |
'product' |
'created' |
'modified'
> {
  ref_id: string;
}

export const IsDeviceRefId = IsDeviceExternalId;
