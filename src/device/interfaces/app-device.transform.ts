import { convertType, Device } from '@swypeless/core';
import { AppDevice } from './app-device.interface';

export function deviceToAppDevice(device: Device): AppDevice {
  return convertType<AppDevice, Device>(device, {
    includeKeys: [
      'id',
      'object',
      'type',
      'os_type',
      'os_name',
      'os_version',
      'manufacturer',
      'model',
      'product',
      'created',
      'modified',
    ],
    addValues: {
      ref_id: device.external_id,
    },
  });
}
