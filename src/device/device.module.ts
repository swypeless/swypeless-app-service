import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { InternalServicesModule } from '@swypeless/microservice-core';
import { DeviceController } from './device.controller';
import { DeviceService } from './device.service';

@Module({
  imports: [
    ConfigModule,
    InternalServicesModule,
  ],
  providers: [
    DeviceService,
  ],
  controllers: [
    DeviceController,
  ],
  exports: [
    DeviceService,
  ],
})
export class DeviceModule {}
