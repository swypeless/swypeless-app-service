import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  deviceLikeToDevice,
  DeviceProvider,
  DEVICE_TYPE_NAME,
  ItemNotFoundException,
} from '@swypeless/core';
import { AbstractService, DEVICE_PROVIDER } from '@swypeless/microservice-core';
import {
  AppDevice,
  AppDeviceUpdateOptions,
  appDeviceUpdateOptionsToDeviceLike,
  deviceToAppDevice,
} from './interfaces';

@Injectable()
export class DeviceService extends AbstractService {
  constructor(
    config: ConfigService,
    @Inject(DEVICE_PROVIDER) private readonly deviceProvider: DeviceProvider,
  ) {
    super(config);
  }

  async get(device_id: string): Promise<AppDevice> {
    const device = await this.deviceProvider.getByPrimaryKey(device_id);
    if (device === undefined) {
      throw new ItemNotFoundException(DEVICE_TYPE_NAME, device_id);
    }
    return deviceToAppDevice(device);
  }

  async update(device_id: string, options: AppDeviceUpdateOptions): Promise<AppDevice> {
    // verify device exists befor updating
    const device = await this.deviceProvider.getByPrimaryKey(device_id);
    if (device === undefined) {
      throw new ItemNotFoundException(DEVICE_TYPE_NAME);
    }
    const updatedDevice = await this.deviceProvider.update(
      deviceLikeToDevice(
        appDeviceUpdateOptionsToDeviceLike(
          options,
          device.external_id,
          device.status,
        ),
        device_id,
      ),
    );
    return deviceToAppDevice(updatedDevice);
  }
}
