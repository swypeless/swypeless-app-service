import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { InternalServicesModule } from '@swypeless/microservice-core';
import { PriceController } from './price.controller';
import { PriceService } from './price.service';

@Module({
  imports: [
    ConfigModule,
    InternalServicesModule,
  ],
  providers: [
    PriceService,
  ],
  controllers: [
    PriceController,
  ],
  exports: [
    PriceService,
  ],
})
export class PriceModule {}
