import { ApiProperty } from '@nestjs/swagger';
import { DeletedPriceDTO, PRICE_OBJECT } from '@swypeless/core';

export class DeletedAppPriceDTO extends DeletedPriceDTO {
  @ApiProperty({
    description: 'Price id',
    example: 'price_5xhgEJqMW7Ldcly4lVVspC',
  })
  id: string;

  @ApiProperty({
    description: 'Object type',
    example: PRICE_OBJECT,
  })
  object: string;

  @ApiProperty({
    description: 'Whether the price was deleted',
    example: true,
  })
  deleted: boolean;
}
