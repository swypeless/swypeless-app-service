import { ApiProperty } from '@nestjs/swagger';
import { IsPriceCurrency, IsPriceId, MutableOrderedListOptionsDTO } from '@swypeless/core';
import { IsOptional } from 'class-validator';
import { AppPriceGetOptions } from '../interfaces';

export class AppPriceGetOptionsDTO
  extends MutableOrderedListOptionsDTO
  implements AppPriceGetOptions {
  @ApiProperty({
    required: false,
    description: 'Price id',
    example: 'price_5xhgEJqMW7Ldcly4lVVspC',
  })
  @IsOptional()
  @IsPriceId()
  id?: string;

  @ApiProperty({
    required: false,
    description: 'Price currency',
    example: 'usd',
  })
  @IsOptional()
  @IsPriceCurrency()
  currency?: string;
}
