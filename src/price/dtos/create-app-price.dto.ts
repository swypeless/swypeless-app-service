import { ApiProperty } from '@nestjs/swagger';
import { DTO, IsPriceCurrency } from '@swypeless/core';
import { AppPriceCreateOptions, IsAppPriceUnitAmount } from '../interfaces';

export class CreateAppPriceDTO extends DTO implements AppPriceCreateOptions {
  @ApiProperty({
    description: 'Price currency',
    example: 'usd',
  })
  @IsPriceCurrency()
  currency: string;

  @ApiProperty({
    description: 'Price unit amount in minimum currency units (mcus)',
    example: 500,
  })
  @IsAppPriceUnitAmount()
  unit_amount: number;
}
