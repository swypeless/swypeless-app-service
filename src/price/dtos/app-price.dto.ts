import { ApiProperty } from '@nestjs/swagger';
import {
  DTOPrimaryKey,
  IsCreated,
  IsModified,
  IsPriceCurrency,
  IsPriceId,
  IsPriceObject,
  IsProductId,
  PRICE_OBJECT,
  PRICE_PRIMARY_KEY,
  ResponseDTO,
  TransformDate,
} from '@swypeless/core';
import { AppPrice, IsAppPriceUnitAmount } from '../interfaces';

@DTOPrimaryKey(PRICE_PRIMARY_KEY)
export class AppPriceDTO extends ResponseDTO implements AppPrice {
  @ApiProperty({
    description: 'Price id',
    example: 'price_5xhgEJqMW7Ldcly4lVVspC',
  })
  @IsPriceId()
  id: string;

  @ApiProperty({
    description: 'Object type',
    example: PRICE_OBJECT,
  })
  @IsPriceObject()
  object: string;

  @ApiProperty({
    type: Number,
    description: 'Time price was created (unix)',
    example: 1626889787,
  })
  @IsCreated()
  @TransformDate
  created: Date;

  @ApiProperty({
    description: 'Price currency',
    example: 'usd',
  })
  @IsPriceCurrency()
  currency: string;

  @ApiProperty({
    type: Number,
    description: 'Time price was last modified (unix)',
    example: 1626889787,
  })
  @IsModified()
  @TransformDate
  modified: Date;

  @ApiProperty({
    description: 'The id of the product this price is associated with.',
    example: 'prod_IcPmfuh1Ye6jGl',
  })
  @IsProductId()
  product_id: string;

  @ApiProperty({
    description: 'The unit amount in minimum currency units (mcus).',
    example: 500,
  })
  @IsAppPriceUnitAmount()
  unit_amount: number;
}
