import { ApiProperty } from '@nestjs/swagger';
import { DataListDTO, DATA_LIST_OBJECT } from '@swypeless/core';
import { AppPriceDTO } from './app-price.dto';

export class AppPriceDataListDTO extends DataListDTO<AppPriceDTO> {
  @ApiProperty({
    description: 'Object type',
    example: DATA_LIST_OBJECT,
  })
  object: string;

  @ApiProperty({
    description: 'Array of elements',
    type: [AppPriceDTO],
  })
  data: AppPriceDTO[];

  @ApiProperty({
    description: 'Whether or not there are more prices available after this set.',
    example: false,
  })
  has_more: boolean;
}
