import { convertType } from '@swypeless/core';
import { AppPrice } from '../interfaces';
import { AppPriceDTO } from './app-price.dto';

export function appPriceToDto(appPrice: AppPrice): AppPriceDTO {
  return convertType<AppPriceDTO, AppPrice>(appPrice, {
    outputBase: new AppPriceDTO(),
  });
}
