import { MutableOrderedListOptions, PriceGetOptions } from '@swypeless/core';

export interface AppPriceGetOptions extends MutableOrderedListOptions, Pick<PriceGetOptions,
'id' |
'currency'
> {}
