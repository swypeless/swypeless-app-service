import { AppPrice } from './app-price.interface';

export type AppPriceCreateOptions = Pick<AppPrice,
'currency' |
'unit_amount'
>;
