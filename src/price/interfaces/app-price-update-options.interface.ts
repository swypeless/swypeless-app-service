import { AppPrice } from './app-price.interface';

export type AppPriceUpdateOptions = Pick<AppPrice,
'currency' |
'unit_amount'
>;
