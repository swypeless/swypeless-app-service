import { defaultPriceValues, Price, priceLikeToPrice } from '@swypeless/core';
import { AppPriceUpdateOptions } from './app-price-update-options.interface';

export function appPriceUpdateOptionsToPrice(
  options: AppPriceUpdateOptions,
  merchant_id: string,
  product_id: string,
  id: string,
): Price {
  return priceLikeToPrice({
    ...defaultPriceValues,
    ...options,
    merchant_id,
    product_id,
  }, id);
}
