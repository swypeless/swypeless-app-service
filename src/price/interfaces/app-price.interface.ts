import { combinePropertyDecorators, Price, STRIPE_MAX_AMOUNT } from '@swypeless/core';
import {
  IsInt, Max, Min, ValidationOptions,
} from 'class-validator';

export interface AppPrice extends Pick<Price,
'id' |
'object' |
'created' |
'currency' |
'modified' |
'product_id'
> {
  unit_amount: number;
}

export const IsAppPriceUnitAmount = (options?: ValidationOptions) => combinePropertyDecorators([
  IsInt(options),
  Min(0, options),
  Max(STRIPE_MAX_AMOUNT, options),
]);
