import { convertType, defaultPriceValues, Price } from '@swypeless/core';
import { AppPrice } from './app-price.interface';

export function priceToAppPrice(price: Price): AppPrice {
  return convertType<AppPrice, Price>(price, {
    includeKeys: [
      'id',
      'object',
      'created',
      'currency',
      'modified',
      'product_id',
      'unit_amount',
    ],
  });
}

export function appPriceToPrice(appPrice: AppPrice, merchant_id: string): Price {
  return convertType<Price, AppPrice>(appPrice, {
    addValues: {
      ...defaultPriceValues,
      merchant_id,
    },
  });
}
