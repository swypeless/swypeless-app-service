import { defaultPriceValues, Price, priceLikeToPrice } from '@swypeless/core';
import { AppPriceCreateOptions } from './app-price-create-options.interface';

export function appPriceCreateOptionsToPrice(
  options: AppPriceCreateOptions,
  merchant_id: string,
  product_id: string,
): Price {
  return priceLikeToPrice({
    ...defaultPriceValues,
    ...options,
    merchant_id,
    product_id,
  });
}
