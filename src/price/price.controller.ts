import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  DataListDTO,
  dataListToDTO,
  DeletedPriceDTO,
  deletedPriceToDTO,
  ItemNotFoundException,
  mutableOrderableProperties,
  OptionsNotValidException,
  PRICE_TYPE_NAME,
} from '@swypeless/core';
import { addOrderableProperties } from '@swypeless/microservice-core/dist/swagger';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { JwtAuthGuard } from '../auth';
import { MerchantId } from '../decorators';
import {
  AppPriceDataListDTO,
  AppPriceDTO,
  AppPriceGetOptionsDTO,
  appPriceToDto,
  CreateAppPriceDTO,
  UpdateAppPriceDTO,
} from './dtos';
import { DeletedAppPriceDTO } from './dtos/deleted-app-price.dto';
import { PriceService } from './price.service';

@ApiTags('Prices')
@ApiBearerAuth()
@Controller('prices')
@UseGuards(JwtAuthGuard)
export class PriceController {
  constructor(
    @InjectPinoLogger(PriceController.name) private readonly logger: PinoLogger,
    private readonly priceService: PriceService,
  ) {}

  @Get()
  @HttpCode(200)
  @ApiOperation({
    summary: 'List Product Prices',
    description: addOrderableProperties(
      'Returns a list of prices for a product.',
      mutableOrderableProperties,
    ),
  })
  @ApiResponse({
    status: 200,
    description: 'List of prices',
    type: AppPriceDataListDTO,
  })
  @ApiResponse({
    status: 400,
    description: 'List options not valid',
  })
  @ApiResponse({
    status: 404,
    description: 'Product not found',
  })
  async find(
    @MerchantId() merchant_id: string,
      @Param('product_id') product_id: string,
      @Query() options: AppPriceGetOptionsDTO,
  ): Promise<DataListDTO<AppPriceDTO>> {
    try {
      const appPrices = await this.priceService.get(merchant_id, product_id, options);
      return dataListToDTO(appPrices, appPriceToDto);
    } catch (error) {
      this.logger.error(
        'Failed to get price list (%s, %s): %o %o',
        merchant_id,
        product_id,
        options,
        error,
      );
      throw this.processError(error);
    }
  }

  @Get(':id')
  @HttpCode(200)
  @ApiOperation({
    summary: 'Get Product Price',
    description: 'Returns a price for product',
  })
  @ApiResponse({
    status: 200,
    description: 'Price object',
    type: AppPriceDTO,
  })
  @ApiResponse({
    status: 404,
    description: 'Product or price not found',
  })
  async findOne(
    @MerchantId() merchant_id: string,
      @Param('product_id') product_id: string,
      @Param('id') id: string,
  ): Promise<AppPriceDTO> {
    try {
      const price = await this.priceService.getById(merchant_id, product_id, id);
      if (price === undefined) {
        throw new NotFoundException(PRICE_TYPE_NAME);
      }
      return appPriceToDto(price);
    } catch (error) {
      this.logger.error('Failed to get price (%s, %s, %s): %o', merchant_id, product_id, id, error);
      throw this.processError(error);
    }
  }

  @Post()
  @HttpCode(201)
  @ApiOperation({
    summary: 'Create Product Price',
    description: 'Creates a price for product',
  })
  @ApiConsumes('application/json')
  @ApiBody({
    description: 'Provide price options.',
    type: CreateAppPriceDTO,
    required: true,
  })
  @ApiResponse({
    status: 201,
    description: 'Price object that was created',
    type: AppPriceDTO,
  })
  @ApiResponse({
    status: 400,
    description: 'Price options not valid',
  })
  @ApiResponse({
    status: 404,
    description: 'Product not found',
  })
  async create(
    @MerchantId() merchant_id: string,
      @Param('product_id') product_id: string,
      @Body() options: CreateAppPriceDTO,
  ): Promise<AppPriceDTO> {
    try {
      const price = await this.priceService.add(merchant_id, product_id, options);
      return appPriceToDto(price);
    } catch (error) {
      this.logger.error(
        'Failed to create price (%s, %s): %o %o',
        merchant_id,
        product_id,
        options,
        error,
      );
      throw this.processError(error);
    }
  }

  @Put(':id')
  @HttpCode(200)
  @ApiOperation({
    summary: 'Update Product Price',
    description: 'Updates a price for product',
  })
  @ApiConsumes('application/json')
  @ApiBody({
    description: 'Provide price options.',
    type: UpdateAppPriceDTO,
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Price object that was updated',
    type: AppPriceDTO,
  })
  @ApiResponse({
    status: 400,
    description: 'Price options not valid',
  })
  @ApiResponse({
    status: 404,
    description: 'Product or price not found',
  })
  async update(
    @MerchantId() merchant_id: string,
      @Param('product_id') product_id: string,
      @Param('id') id: string,
      @Body() options: UpdateAppPriceDTO,
  ): Promise<AppPriceDTO> {
    try {
      const price = await this.priceService.update(merchant_id, product_id, id, options);
      return appPriceToDto(price);
    } catch (error) {
      this.logger.error(
        'Failed to update price (%s, %s, %s): %o %o',
        merchant_id,
        product_id,
        id,
        options,
        error,
      );
      throw this.processError(error);
    }
  }

  @Delete(':id')
  @HttpCode(200)
  @ApiOperation({
    summary: 'Delete Product Price',
    description: 'Deletes a price for product',
  })
  @ApiResponse({
    status: 200,
    description: 'Deleted price object',
    type: DeletedAppPriceDTO,
  })
  @ApiResponse({
    status: 404,
    description: 'Product or price not found',
  })
  async remove(
    @MerchantId() merchant_id: string,
      @Param('product_id') product_id: string,
      @Param('id') id: string,
  ): Promise<DeletedPriceDTO> {
    try {
      const deletedPrice = await this.priceService.delete(merchant_id, product_id, id);
      return deletedPriceToDTO(deletedPrice);
    } catch (error) {
      this.logger.error(
        'Failed to delete price (%s, %s, %s): %o',
        merchant_id,
        product_id,
        id,
        error,
      );
      throw this.processError(error);
    }
  }

  private processError(error: Error): Error {
    switch (error.constructor) {
      case ItemNotFoundException:
        return new NotFoundException();
      case OptionsNotValidException:
        return new BadRequestException();
      default:
        break;
    }
    return error;
  }
}
