import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  convertDataList,
  DataList,
  DeletedPrice,
  ItemNotFoundException,
  PriceProvider,
  PRICE_TYPE_NAME,
  ProductProvider,
  PRODUCT_TYPE_NAME,
} from '@swypeless/core';
import { AbstractService, PRICE_PROVIDER, PRODUCT_PROVIDER } from '@swypeless/microservice-core';
import {
  AppPrice,
  AppPriceCreateOptions,
  appPriceCreateOptionsToPrice,
  AppPriceUpdateOptions,
  appPriceUpdateOptionsToPrice,
  priceToAppPrice,
  AppPriceGetOptions,
} from './interfaces';

@Injectable()
export class PriceService extends AbstractService {
  constructor(
    config: ConfigService,
    @Inject(PRICE_PROVIDER) private readonly priceProvider: PriceProvider,
    @Inject(PRODUCT_PROVIDER) private readonly productProvider: ProductProvider,
  ) {
    super(config);
  }

  async get(
    merchant_id: string,
    product_id: string,
    options: AppPriceGetOptions,
  ): Promise<DataList<AppPrice>> {
    const prices = await this.priceProvider.get({
      ...options,
      merchant_id,
      product_id,
    });
    return convertDataList(prices, priceToAppPrice);
  }

  async getById(
    merchant_id: string,
    product_id: string,
    id: string,
  ): Promise<AppPrice | undefined> {
    const prices = await this.get(merchant_id, product_id, {
      id,
    });
    return prices.data[0];
  }

  async add(
    merchant_id: string,
    product_id: string,
    options: AppPriceCreateOptions,
  ): Promise<AppPrice> {
    // verify product exists before adding price
    const product = await this.productProvider.getByPrimaryKey(merchant_id, product_id);
    if (product === undefined) {
      throw new ItemNotFoundException(PRODUCT_TYPE_NAME, product_id);
    }
    // add price
    const price = appPriceCreateOptionsToPrice(options, merchant_id, product_id);
    const newPrice = await this.priceProvider.add(price);
    return priceToAppPrice(newPrice);
  }

  async update(
    merchant_id: string,
    product_id: string,
    id: string,
    options: AppPriceUpdateOptions,
  ): Promise<AppPrice> {
    const price = appPriceUpdateOptionsToPrice(options, merchant_id, product_id, id);
    const updatedPrice = await this.priceProvider.update(price);
    return priceToAppPrice(updatedPrice);
  }

  async delete(merchant_id: string, product_id: string, id: string): Promise<DeletedPrice> {
    const price = await this.getById(merchant_id, product_id, id);
    if (price === undefined) {
      throw new ItemNotFoundException(PRICE_TYPE_NAME, id);
    }
    return this.priceProvider.deleteByPrimaryKey(id);
  }
}
