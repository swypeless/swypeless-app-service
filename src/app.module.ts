import {
  HttpClientModule,
  pinoHttpOpts,
  ServiceModule,
  InternalModule,
} from '@swypeless/microservice-core';
import { Logger, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import { LoggerModule } from 'nestjs-pino';
import { RouterModule } from 'nest-router';
import PinoHttp from 'pino-http';
import {
  SERVICE_ENV,
  pinoOptions,
  serviceInfo,
} from './config';
import { nestedRoutes } from './nested-routes';
import { AuthModule } from './auth';
import { AccountModule } from './account';
import { PriceModule } from './price';
import { ProductModule } from './product';
import { CurrencyModule } from './currency';
import { DeviceModule } from './device';
import { SaleModule } from './sale';
import { FeeModule } from './fee';
import { RedemptionModule } from './redemption';
import { ReferralModule } from './referral';

const validationSchema: any = {
  NODE_ENV: Joi.string()
    .valid('development', 'production', 'e2e', 'test')
    .default('development'),
  SERVICE_ENV: Joi.string()
    .valid('local', 'stage', 'production')
    .default('local'),
};
if (SERVICE_ENV === 'local') {
  Logger.log('Adding AWS env vars for running in local environment', 'AppModule');
  validationSchema.AWS_ACCOUNT = Joi.number()
    .precision(0)
    .required();
  validationSchema.AWS_ACCESS_KEY_ID = Joi.string().required();
  validationSchema.AWS_SECRET_ACCESS_KEY = Joi.string().required();
  validationSchema.AWS_REGION = Joi.string()
    .pattern(/[a-z]{2}-[a-z]+-\d+/)
    .required();
}

const envValidation = Joi.object(validationSchema);

const pinoOpts = pinoHttpOpts(pinoOptions);

if (pinoOpts.pinoHttp && (pinoOpts.pinoHttp as PinoHttp.Options).name) {
  (pinoOpts.pinoHttp as PinoHttp.Options).name = 'access';
  // pinoOpts.useExisting = false;
}

@Module({
  imports: [

    ConfigModule.forRoot({
      expandVariables: true,
      ignoreEnvVars: false,
      isGlobal: true,
      envFilePath: [
        '.env',
        `.env.SERVICE_ENV--${SERVICE_ENV}`, // Use for local/stage/production resources
        `.env.NODE_ENV--${process.env.NODE_ENV}`, // Use for compilation type settings development/production
      ],
      validationOptions: {
        allowUnknown: true,
        abortEarly: false,
      },
      validationSchema: envValidation,
      load: [(): any => ({ serviceInfo })],
    }),
    LoggerModule.forRoot(pinoOpts),

    HttpClientModule,
    ServiceModule,
    InternalModule,

    AuthModule,

    AccountModule,
    DeviceModule,
    PriceModule,
    ProductModule,

    CurrencyModule,

    FeeModule,
    SaleModule,

    RedemptionModule,
    ReferralModule,

    // Nested routes, products
    RouterModule.forRoutes(nestedRoutes),
  ],
})
export class AppModule {

}
