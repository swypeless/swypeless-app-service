import { Currency } from '@swypeless/core';

export type AppCurrency = Pick<Currency,
'id' |
'object' |
'code' |
'created' |
'fraction_digits' |
'minimum_amount' |
'modified' |
'name'
>;
