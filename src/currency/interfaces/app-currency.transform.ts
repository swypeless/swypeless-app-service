import { convertType, Currency } from '@swypeless/core';
import { AppCurrency } from './app-currency.interface';

export function currencyToAppCurrency(currency: Currency): AppCurrency {
  return convertType<AppCurrency, Currency>(currency, {
    includeKeys: [
      'id',
      'object',
      'code',
      'created',
      'fraction_digits',
      'minimum_amount',
      'modified',
      'name',
    ],
  });
}
