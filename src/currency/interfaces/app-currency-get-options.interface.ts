import { CurrencyGetOptions, MutableOrderedListOptions } from '@swypeless/core';

export interface AppCurrencyGetOptions extends MutableOrderedListOptions, Pick<CurrencyGetOptions,
'id' |
'code'
> {}
