import { convertType, CurrencyGetOptions, CurrencyStatus } from '@swypeless/core';
import { AppCurrencyGetOptions } from './app-currency-get-options.interface';

export function appCurrencyGetOptionsToCurrencyGetOptions(
  options: AppCurrencyGetOptions,
): CurrencyGetOptions {
  return convertType<CurrencyGetOptions, AppCurrencyGetOptions>(options, {
    addValues: {
      status: CurrencyStatus.Active,
    },
  });
}
