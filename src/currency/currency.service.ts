import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  convertDataList,
  CurrencyProvider,
  CurrencyStatus,
  DataList,
} from '@swypeless/core';
import { AbstractService, CURRENCY_PROVIDER } from '@swypeless/microservice-core';
import {
  AppCurrency,
  AppCurrencyGetOptions,
  currencyToAppCurrency,
} from './interfaces';

@Injectable()
export class CurrencyService extends AbstractService {
  constructor(
    config: ConfigService,
    @Inject(CURRENCY_PROVIDER) private readonly currencyProvider: CurrencyProvider,
  ) {
    super(config);
  }

  async get(options: AppCurrencyGetOptions): Promise<DataList<AppCurrency>> {
    const currencies = await this.currencyProvider.get({
      ...options,
      status: CurrencyStatus.Active,
    });
    return convertDataList(currencies, currencyToAppCurrency);
  }
}
