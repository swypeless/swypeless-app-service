import { convertType } from '@swypeless/core';
import { AppCurrency } from '../interfaces';
import { AppCurrencyDTO } from './app-currency.dto';

export function appCurrencyToDto(appCurrency: AppCurrency): AppCurrencyDTO {
  return convertType<AppCurrencyDTO, AppCurrency>(appCurrency, {
    outputBase: new AppCurrencyDTO(),
  });
}
