import { ApiProperty } from '@nestjs/swagger';
import {
  CURRENCY_OBJECT,
  CURRENCY_PRIMARY_KEY,
  DTOPrimaryKey,
  IsCreated,
  IsCurrencyCode,
  IsCurrencyFractionDigits,
  IsCurrencyId,
  IsCurrencyMinimumAmount,
  IsCurrencyName,
  IsCurrencyObject,
  IsModified,
  ResponseDTO,
  TransformDate,
} from '@swypeless/core';
import { AppCurrency } from '../interfaces';

@DTOPrimaryKey(CURRENCY_PRIMARY_KEY)
export class AppCurrencyDTO extends ResponseDTO implements AppCurrency {
  @ApiProperty({
    description: 'Currency id',
    example: 'cur_IcPmfuh1Ye6jGl',
  })
  @IsCurrencyId()
  id: string;

  @ApiProperty({
    description: 'Object type',
    example: CURRENCY_OBJECT,
  })
  @IsCurrencyObject()
  object: string;

  @ApiProperty({
    description: 'Three-letter ISO currency code, in lowercase.',
    example: 'usd',
  })
  @IsCurrencyCode()
  code: string;

  @ApiProperty({
    type: Number,
    description: 'Time price was created (unix)',
    example: 1626889787,
  })
  @IsCreated()
  @TransformDate
  created: Date;

  @ApiProperty({
    description: 'Number of fraction or decimal digits for the currency.',
    example: 2,
  })
  @IsCurrencyFractionDigits()
  fraction_digits: number;

  @ApiProperty({
    description: 'Minimum amount a sale can be using this currency, in minimum currency units (mcus).',
    example: 100,
  })
  @IsCurrencyMinimumAmount()
  minimum_amount: number;

  @ApiProperty({
    type: Number,
    description: 'Time price was created (unix)',
    example: 1626889787,
  })
  @IsModified()
  @TransformDate
  modified: Date;

  @ApiProperty({
    description: 'Name of currency',
    example: 'United States Dollar',
  })
  @IsCurrencyName()
  name: string;
}
