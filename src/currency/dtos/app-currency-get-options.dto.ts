import { ApiProperty } from '@nestjs/swagger';
import {
  ForbidsProperties,
  IsCurrencyCode,
  IsCurrencyId,
  MutableOrderedListOptionsDTO,
} from '@swypeless/core';
import { IsOptional } from 'class-validator';
import { AppCurrencyGetOptions } from '../interfaces';

export class AppCurrencyGetOptionsDTO
  extends MutableOrderedListOptionsDTO
  implements AppCurrencyGetOptions {
  @ApiProperty({
    required: false,
    description: 'Currency id, forbids using code parameter',
    example: 'cur_IcPmfuh1Ye6jGl',
  })
  @IsOptional()
  @ForbidsProperties(['code'])
  @IsCurrencyId()
  id?: string;

  @ApiProperty({
    required: false,
    description: 'Currency code, forbids using id parameter',
    example: 'usd',
  })
  @IsOptional()
  @ForbidsProperties(['id'])
  @IsCurrencyCode()
  code?: string;
}
