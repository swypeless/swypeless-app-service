import { ApiProperty } from '@nestjs/swagger';
import { DataListDTO, DATA_LIST_OBJECT } from '@swypeless/core';
import { AppCurrencyDTO } from './app-currency.dto';

export class AppCurrencyDataListDTO extends DataListDTO<AppCurrencyDTO> {
  @ApiProperty({
    description: 'Object type',
    example: DATA_LIST_OBJECT,
  })
  object: string;

  @ApiProperty({
    description: 'Array of elements',
    type: [AppCurrencyDTO],
  })
  data: AppCurrencyDTO[];

  @ApiProperty({
    description: 'Whether or not there are more currencies available after this set.',
    example: false,
  })
  has_more: boolean;
}
