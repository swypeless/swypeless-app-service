import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { InternalServicesModule } from '@swypeless/microservice-core';
import { CurrencyController } from './currency.controller';
import { CurrencyService } from './currency.service';

@Module({
  imports: [
    ConfigModule,
    InternalServicesModule,
  ],
  providers: [
    CurrencyService,
  ],
  controllers: [
    CurrencyController,
  ],
  exports: [
    CurrencyService,
  ],
})
export class CurrencyModule {}
