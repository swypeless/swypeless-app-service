import {
  BadRequestException,
  Controller,
  Get,
  HttpCode,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { DataListDTO, dataListToDTO, OptionsNotValidException } from '@swypeless/core';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { JwtAuthGuard } from '../auth';
import { CurrencyService } from './currency.service';
import {
  AppCurrencyDataListDTO,
  AppCurrencyDTO,
  AppCurrencyGetOptionsDTO,
  appCurrencyToDto,
} from './dtos';

@ApiTags('Currencies')
@ApiBearerAuth()
@Controller('currencies')
@UseGuards(JwtAuthGuard)
export class CurrencyController {
  constructor(
    @InjectPinoLogger(CurrencyController.name) private readonly logger: PinoLogger,
    private readonly currencyService: CurrencyService,
  ) {}

  @Get()
  @HttpCode(200)
  @ApiOperation({
    summary: 'List Currencies',
    description: 'Returns a list of active currencies',
  })
  @ApiResponse({
    status: 200,
    description: 'List of currencies',
    type: AppCurrencyDataListDTO,
  })
  @ApiResponse({
    status: 400,
    description: 'List options not valid',
  })
  async find(@Query() options: AppCurrencyGetOptionsDTO): Promise<DataListDTO<AppCurrencyDTO>> {
    try {
      const appCurrencies = await this.currencyService.get(options);
      return dataListToDTO(appCurrencies, appCurrencyToDto);
    } catch (error) {
      this.logger.error('Failed to get currency list: %o %o', options, error);
      switch (error.constructor) {
        case OptionsNotValidException:
          throw new BadRequestException();
        default:
          break;
      }
      throw error;
    }
  }
}
