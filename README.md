# App Service #

Backend service to the app, primary api

## Types of requests

This service handles the following:

- Providing connection url
- App backend api

## Check system requirements

Run `check-my-system` to check system requirements:
```
check-my-system
```

If everything is configured correctly, there should be no issues with the `$PATH` check and the environment variables check.  If there are all of these need to be resolved.

Any missing tools need to be installed, however, version issues need only be resolved if the required version is newer or incompatible with the installed version.

Check the setup from the `swypeless-dev-env` repo for general setup issues.

## Environment Variables

### General

| Variable                  	| Required 	| Type    	| Example                                    	| Notes                                                             	|
|---------------------------	|:--------:	|---------	|--------------------------------------------	|-------------------------------------------------------------------	|
| AWS_REGION                	|    Yes   	| string  	| us-east-1                                  	| Must be a valid AWS Region identifier                             	|
| PORT                      	|    No    	| integer 	| 3000 (default)                             	| See Port in Local Environment Below                               	|
| BASE_PATH                 	|    No    	| string  	| [empty] (default)                          	| Base path of service api from host                                	|
| SERVICE_ENV               	|    No    	| string  	| local (default)                            	| Can be 'local', 'stage' or 'prod'                                 	|
| DEBUG                     	|    No    	| boolean 	| true                                       	| false by default                                                  	|
| AUTH_CONNECT_REDIRECT_URI 	|    No*   	| string  	| http://stripe.localhost:3000/connect/oauth 	| Redirect uri for connection flow, * required in local environment 	|

<br />

**Port in Local Environment**

You will need to define the port variable when using the local proxy, however, you will need to define it by using the export method (see Adding Variables to your Environment below).

### Internal Services

| Variable             | Required | Type    | Example        | Notes                                              |
|----------------------|:--------:|---------|----------------|----------------------------------------------------|
| INTERNAL_URL_SCHEME  |    No    | string  | http (default) | Currently, only http is used in all environments   |
| INTERNAL_URL_ROOT    |    Yes   | string  | localhost:3000 | Pulled from hosted zone in stage and prod          |
| AXIOS_PROXY_HOST     |    No    | string  | localhost      | Host for Axios proxy, needed in local              |
| AXIOS_PROXY_PORT     |    No    | integer | 3000           | Port for Axios proxy, need in local                |
| AXIOS_PROXY_PROTOCOL |    No    | string  | http           | Protocol for Axios proxy, could be needed in local |

<br />

### Authorization & Credentials

| Variable                      	| Required 	| Type      	| Example                	| Notes                                                        	|
|-------------------------------	|:--------:	|-----------	|------------------------	|--------------------------------------------------------------	|
| JWT_ISSUER                    	|    Yes   	| URL       	| https://swypeless.com/ 	| Must match issuer variables of other services in environment 	|
| JWT_EXPIRES_IN                	|    No    	| time span 	| 60s (default)          	| See [ms](https://www.npmjs.com/package/ms) for time spans    	|
| JWT_AUDIENCE                  	|    Yes  	| string    	| app-service            	| Must match APP_CONNECTION_JWT_AUDIENCE in internal service   	|

<br />

### Cache Config

**Local Cache**

Local in-memory cache config.  *NOT* related to a local environment or any environment.  Valid in all environments.

| Variable        	| Required 	| Type    	| Example       	| Notes                                   	|
|-----------------	|:--------:	|---------	|---------------	|-----------------------------------------	|
| LOCAL_CACHE_MAX 	|    No    	| integer 	| 100 (default) 	| Max items in local cache                	|
| LOCAL_CACHE_TTL 	|    No    	| integer 	| 60 (default)  	| TTL of items in local cache, in seconds 	|

<br />

**Remote Cache**

Remote shared cache.  *NOT* related to any specific environment, but probably only used in stage or prod environments.  Valid in all environments.  **\*\* NO REMOTE CACHE AVAILABLE CURRENTLY**

| Variable                                      	| Required 	| Type    	| Example             	| Notes                                                                                     	|
|-----------------------------------------------	|:--------:	|---------	|---------------------	|-------------------------------------------------------------------------------------------	|
| REMOTE_CACHE_MAX                              	|    No    	| integer 	| 10000 (default)     	| Max items in local cache                                                                  	|
| REMOTE_CACHE_TTL                              	|    No    	| integer 	| 300 (default)       	| TTL of items in remote cache, in seconds, default 5 mins                                  	|
| REMOTE_CACHE_TYPE                             	|   Yes*   	| string  	| elasticache         	| Must be 'elasticache' or 'redis'. *Required if configured in environment.                 	|
| REMOTE_CACHE_ELASTICACHE_REPLICATION_GROUP_ID 	|   Yes*   	| string  	|                     	| Amazon elasticache replication group id. *Required if REMOTE_CACHE_TYPE is 'elasticache'. 	|
| REMOTE_CACHE_REDIS_HOST                       	|    No    	| string  	| localhost (default) 	| Host of redis cache                                                                       	|
| REMOTE_CACHE_REDIS_PORT                       	|    No    	| integer 	| 6379 (default)      	| Port of redis cache                                                                       	|

<br />

**Secrets Cache**

Secrets are kept in a separate in-memory cache.  Valid in all environments.

| Variable          	| Required 	| Type    	| Example       	| Notes                                     	|
|-------------------	|:--------:	|---------	|---------------	|-------------------------------------------	|
| SECRET_CACHE_MAX   	|    No    	| integer 	| 100 (default) 	| Max items in secret cache                  	|
| SECRET_CACHE_TTL  	|    No    	| integer 	| 60 (default)  	| TTL of items in secret cache, in seconds  	|

<br />

**Local Secrets**

Local secrets are required in local environment only.  Secrets in stage and prod environments are stored in a separate service.

| Variable           	| Required 	| Type   	| Example                  	| Notes                          	|
|--------------------	|:--------:	|--------	|--------------------------	|--------------------------------	|
| LOCAL_SECRETS_FILE 	|    Yes   	| string 	| ${PWD}/temp/secrets.json 	| File containing local secrets. 	|

<br />

## Adding Variables to your Environment

There are three methods to add variables to the environment where your app is running.

**1. Manually Export the Variable to your Environment**

Enter into your terminal, replacing the variable name and value:

```
export VARIABLE=value
```

This method is not recommended because the terminal does not save your variables accross sessions.

**2. Export the Variable to your Environment using `direnv`**

Use `direnv` ([direnv](https://direnv.net/)) to manage environment variables in `.envrc` files that are ignored by `.gitignore`.

This method is suggested for static variables and variables that will be shared accross projects.

**3. Use a `.env` file locally**

This service loads environment variables from `.env` files from three filename formats:

- `.env`
- `.env.SERVICE_ENV--[environment]`
- `.env.NODE_ENV--[environment]`

Replace '`[environment]`' with the service and node environments respectively.

This can be used to separate environment specific variable values.

*Service environments:*

- `local`
- `stage`
- `production`

*Node environments:*

- `development`
- `production`

## Secrets

| Secret Key                       	| Type   	| Value Example        	| Notes                           	|
|----------------------------------	|--------	|----------------------	|---------------------------------	|
| stripe/[environment]/client-id  	| string 	| ca_2jkfdj...f0u4jf0 	| Stripe client id, use test client	|
| jwt/[environment]/signing-secret 	| string 	| sdjf43...20jjf98     	| Signing secret for jwt signing  	|

<br />

### Secrets in Local Environment

Secrets in the local environment are stored in a json file specified in the `LOCAL_SECRETS_FILE` environment variable.

Currently, the structure of both stripe and jwt secrets are as follows:
```
{
  secret: "[secret value]"
}
```

They are represented in the json file as follows:
```
{
  "[secret key]": {
    "secret": "[secret value]"
  }
}
```

**Example Local Secrets File**

secrets.json
```
{
  "stripe/local/client-id": {
    "secret": "[public key]"
  },
  "jwt/local/signing-secret": {
    "secret": "[signing secret]"
  }
}
```

## Infrastructure

Manage this service's infrastructure using the `tf` utility only.  See the Swypeless Development Environment repository `README.md` for more information on using the `tf` utility.

### Shared Resources

- ecr: Elastic Container Registry, contains docker images for ecs

### Distinct Recources

- lb: Application Load Balancer
- service: Elastic Container Service service

## Deployment

Deploy this service using the `dkr` utility.  See the Swypeless Development Environment repository `README.md` for more information on using the `dkr` utility.